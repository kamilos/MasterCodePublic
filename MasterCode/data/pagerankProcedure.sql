
CREATE PROCEDURE PageRank()
BEGIN   

	declare DampingFactor decimal(3,2) DEFAULT 0.85; -- set the damping factor
    declare MarginOfError decimal(10,5) DEFAULT 0.001; -- set the stable weight
    declare TotalNodeCount int ;
    declare IterationCount int DEFAULT 1 ;
    declare iteration int DEFAULT 1 ;

    set TotalNodeCount = (select count(*) from node); -- we need to know the total number of nodes in the system
    
    -- compte le nombre de noeuds sortant pour un noeud source node donné
	UPDATE NodesPR AS n
	LEFT OUTER join (SELECT SourceNodeID, 
							count(*) as TargetNodeCount 
							FROM EdgesPR
							GROUP BY SourceNodeId) as x on x.SourceNodeID = n.NodeId
	SET n.NodeCount = ifnull(x.TargetNodeCount,TotalNodeCount);
   
    WHILE(iteration < 3) DO 
    
		UPDATE NodesPR AS n
        LEFT JOIN ( -- Here's the weight calculation in place
			SELECT
				e.TargetNodeId, (sum(n.NodeWeight / n.NodeCount) * DampingFactor) as TransferredNodeWeight 
			FROM NodesPR n
			INNER JOIN EdgesPR e
				ON n.NodeId = e.SourceNodeId
			GROUP BY e.TargetNodeId
		) as x ON x.TargetNodeId = n.NodeId
        SET NodeWeight = 1.0 - DampingFactor + ifnull(x.TransferredNodeWeight, 0.0);

		

        set iteration = iteration+1;
	END WHILE;
    
	select TotalNodeCount;

END; //