CREATE DATABASE IF NOT EXISTS masterDB;
CREATE TABLE IF NOT EXISTS node (
	 name INT,
	 age INT(3),
	 outgoingNodes int default 0,
	 pagerank decimal(10,3) default 0,
	 PRIMARY KEY (name)
) ENGINE=InnoDB CHARSET=utf8;
CREATE TABLE IF NOT EXISTS edge (
	inNode INT  REFERENCES node (name),
	outNode INT  REFERENCES node (name),
	weight INT,
	type varchar(50),
	PRIMARY KEY (inNode,outNode)
) ENGINE=InnoDB CHARSET=utf8;
CREATE INDEX weight_index USING BTREE ON edge (weight);
CREATE INDEX weight_index_hash USING HASH ON edge (weight);
CREATE INDEX age_index USING BTREE ON node (age);
CREATE INDEX outgoingNodes USING HASH ON node (outgoingNodes);
CREATE INDEX pagerank USING HASH ON node (pagerank);
CREATE INDEX type USING HASH ON edge (type);
