SET SQL_SAFE_UPDATES = 0;

drop table if exists NodesPR;
drop table if exists EdgesPR;

CREATE TABLE IF not EXISTs NodesPR
(NodeId int not null
,NodeWeight decimal(10,5) not null
,NodeCount int not null default 0
,HasConverged bit not null default 0
,constraint NodesPK primary key clustered (NodeId)
);

CREATE TABLE IF not EXISTs EdgesPR
(SourceNodeId int not null
,TargetNodeId int not null
,constraint EdgesPK primary key clustered (SourceNodeId, TargetNodeId)
);

INSERT INTO NodesPR (NodeId, NodeWeight) VALUES (1, 0.25),(2, 0.25),(3, 0.25),(4, 0.25);

INSERT INTO EdgesPR (SourceNodeId, TargetNodeId) VALUES (2, 1) ,(2, 3),(3, 1),(4, 1),(4, 2),(4, 3);
DELIMITER //


DROP PROCEDURE IF EXISTS PageRank;//
CREATE PROCEDURE PageRank()
BEGIN   

	declare DampingFactor decimal(3,2) DEFAULT 0.85; -- set the damping factor
    declare MarginOfError decimal(10,5) DEFAULT 0.001; -- set the stable weight
    declare TotalNodeCount int ;
    declare IterationCount int DEFAULT 1 ;
    declare iteration int DEFAULT 1 ;

    set TotalNodeCount = (select count(*) from node); -- we need to know the total number of nodes in the system
    
    -- compte le nombre de noeuds sortant pour un noeud source node donné
	UPDATE NodesPR AS n
	LEFT OUTER join (SELECT SourceNodeID, 
							count(*) as TargetNodeCount 
							FROM EdgesPR
							GROUP BY SourceNodeId) as x on x.SourceNodeID = n.NodeId
	SET n.NodeCount = ifnull(x.TargetNodeCount,TotalNodeCount);
   
    WHILE(iteration < 3) DO 
    
		UPDATE NodesPR AS n
        LEFT JOIN ( -- Here's the weight calculation in place
			SELECT
				e.TargetNodeId, (sum(n.NodeWeight / n.NodeCount) * DampingFactor) as TransferredNodeWeight 
			FROM NodesPR n
			INNER JOIN EdgesPR e
				ON n.NodeId = e.SourceNodeId
			GROUP BY e.TargetNodeId
		) as x ON x.TargetNodeId = n.NodeId
        SET NodeWeight = 1.0 - DampingFactor + ifnull(x.TransferredNodeWeight, 0.0);

		

        set iteration = iteration+1;
	END WHILE;
    
	select TotalNodeCount;


END; //

DELIMITER ;

CALL PageRank();


select * from NodesPR where NodeId=3