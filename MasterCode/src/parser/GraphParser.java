package parser;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import constant.Const;

public class GraphParser {
	
	
	public Map<Integer, String> nodes = new HashMap<>(); //biginteger ou équivalent si problèmes
	public Map<String, String> edges = new HashMap<>();
	public ArrayList<Integer> weight = new ArrayList<Integer>();
	public ArrayList<Integer> age = new ArrayList<Integer>();
	public ArrayList<Integer> testValues = new ArrayList<Integer>();

	int node1;
	int node2;
	public int NbEdges;
	public int NbNodes;

	
	public Map<Integer, ArrayList<Integer>> nodesEdges = new HashMap<>();


	public void GraphParser(){
		
		
	}

	
	public void parse2(String path) throws Exception{

		BufferedReader br = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(path))));
		String currentLine;
		
		String[] pieces;
		ArrayList<Integer> toInsert;
		while ( (currentLine = br.readLine()).startsWith("#") ); //on se débarrasse des #
		
		do{
			pieces = currentLine.split("\t");
			node1= Integer.valueOf(pieces[0]);
			node2= Integer.valueOf(pieces[1]);
			
			toInsert = nodesEdges.get(node1);
			if(toInsert == null){ // pas encore node1 présent
				toInsert = new ArrayList<Integer>();
				NbNodes++;		
			}
			toInsert.add(node2);
			
			nodesEdges.put(node1, toInsert);
			NbEdges++;
			
			toInsert = nodesEdges.get(node2);
			if(toInsert == null){
				toInsert = new ArrayList<Integer>();
				nodesEdges.put(node2, toInsert);
				NbNodes++;		
			}
			
			

		} while ( (currentLine = br.readLine()) != null );
		
		br.close();
	}

	public void loadWeight() throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(Const.WEIGHT_PATH))));
		String currentLine;
		

		while ( (currentLine = br.readLine()) != null ){
			weight.add(Integer.valueOf(currentLine));
		}
		
		br.close();		
	}

	public void loadAge() throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(Const.AGE_PATH))));
		String currentLine;
		
		while ( (currentLine = br.readLine()) != null ){
			age.add(Integer.valueOf(currentLine));

		}
		
		br.close();			
	}

	public void loadTestValues() throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(Const.TEST_VALUES_PATH))));
		String currentLine;
		
		while ( (currentLine = br.readLine()) != null ){
			testValues.add(Integer.valueOf(currentLine));
		}
		
		br.close();			
	}

}
