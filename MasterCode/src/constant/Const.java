package constant;

import java.io.File;

public class Const {
 public static String 
//	SIZE = "google_2_20";
//	public static String NAME = "";
//SIZE = "amazon_2_19";
//public static String NAME = "";
//// 	
 	SIZE = "2_20";
 	public static String NAME = "kronecker_graph_";
// 
 	public static DB_CHOICES 
	DB_CHOICE = DB_CHOICES.NEO4J_CYPHER;

 public static CHOICES 
  	CHOICE = CHOICES.GraphMatchingConditionnal;
 public static boolean 
 	NEW = true;

	public static enum CHOICES {
		SPECIAL,
		  GetNodeByID,
		  GetNodeByProperty,
		  GetNodeByPropertyRange,
		  GetNodeByPropertyRangeNoLimit,
		  GetNodeByPropertyRangeGroupBy,
		  GetEdgeByID,
		  GetEdgeByProperty,
		  GetEdgeByPropertyRange,
		  GetEdgeByPropertyGroupBy,
		  GetNeighborhood,
		  GetNeighborhoodFilter,
		  GetNeighborhoodFilterOrderBy,
		  GetNeighborhoodBoth,
		  GetKhop,
		  GraphMatching,
		  GraphMatchingConditionnal,
		  PageRank,
		  PageRankNative,
		  ShortestPath,
		  ShortestPathWeight,
		  AddNode,
		  DeleteNode,
		  UpdateNode,
		  AddEdge,
		  DeleteEdge,
		  UpdateEdge,
		 
		  PHP;	
		}

	public static enum DB_CHOICES {
		  NEO4J,
		  TITAN,
		  ORIENTDB,
		  ORIENTDBOSQL,
		  NEO4J_CYPHER,
		  AUTO,
		  MYSQL_NATIVE,
		  MYSQL;
		
		}
	
	public static boolean INIT;
	
	public static String SQL_PAGERANK_PATH = new File("").getAbsolutePath() + File.separator + "data" + File.separator + "pagerankProcedure.sql";

	public static String SQL_TABLES_PATH = new File("").getAbsolutePath() + File.separator + "data" + File.separator + "db.sql";
	public static String SQL_ADRESS_ROOT = "jdbc:mysql://localhost:3306/?autoReconnect=true&useSSL=false";
	
	

	public static String TEST_VALUES_PATH = new File("").getAbsolutePath() + File.separator + "data" + File.separator + "testValues.txt";

	public static String WEIGHT_PATH = new File("").getAbsolutePath() + File.separator + "data" + File.separator + "weights1.txt";
	
	public static String DATASET_PATH = new File("").getAbsolutePath() + File.separator + "input" + File.separator +NAME + SIZE+".txt";

	///////////////////
	//DATABASES 
	///////////////// 

	public static final String DB_PATH_Neo4J = new File("").getAbsolutePath() +File.separator + "databases" + File.separator +  File.separator +  "dbNeo4J_"+SIZE;

	public static final String DB_PATH_Titan = new File("").getAbsolutePath() +File.separator + "databases" + File.separator +  "dbTitan_"+SIZE;
	public static final String DB_PATH_Titan_LUCENE_INDEX= new File("")
			.getAbsolutePath() +File.separator + "databases"+ File.separator + "titanSearchIndexLucene_"+SIZE;
	
	public static final String DB_PATH_OrientDB = new File("").getAbsolutePath() +File.separator +"databases" + File.separator +   "dbOrientDB_"+SIZE;

	public static final String DB_PATH_MYSQL = "dbMysql_"+SIZE;
	


	public static final String AGE_PATH = new File("").getAbsolutePath() + File.separator + "data" + File.separator + "ages.txt";
	public static final String AUTO_PATH = new File("").getAbsolutePath() + File.separator + "data" + File.separator + "auto.txt";

	public static String SQL_ADRESS = "jdbc:mysql://localhost:3306/"+DB_PATH_MYSQL+"?autoReconnect=true&useSSL=false";
	public static String SQL_ADRESS_BATCH = "jdbc:mysql://localhost:3306/"+DB_PATH_MYSQL+"?autoReconnect=true&useSSL=false&rewriteBatchedStatements=true"; // va rend 2 plus rapide



}
