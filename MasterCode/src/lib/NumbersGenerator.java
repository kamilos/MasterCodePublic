package lib;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import constant.Const;


public class NumbersGenerator {
	
	public void generateAgeANDWeight(Integer maxNodes, int maxEdges){
		
		try {

			File file = new File(Const.WEIGHT_PATH);

			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			for(int i=0;i<maxEdges;i++){
				bw.write(0+(int)(Math.random() * ((500 - 0) + 1)) + "\n");

			}
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {


			File file = new File(Const.AGE_PATH);

			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			for(int i=0;i<maxNodes;i++){
				bw.write(0+(int)(Math.random() * ((110 - 0) + 1)) + "\n");

			}
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void generateTestValues(int number, int min, int max) {
		System.out.println("Generateur test enclenché !");
		System.out.println("max: "+ max);

		try {
			File file = new File(Const.TEST_VALUES_PATH);

			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			Random randomGenerator = new Random();
			for(int i=0;i<number;i++){
				//bw.write(min+(int)(Math.random() * ((max - min) + min)) + "\n");
				bw.write(randomGenerator.nextInt(max)+ "\n");
				//System.out.println(randomGenerator.nextInt(max));
//				try {
//					Thread.sleep(2);
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}

			}
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	

}
