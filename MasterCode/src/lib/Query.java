package lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



import constant.Const;

public abstract class Query {
	
	protected long time1;
	protected long time2;
	protected ArrayList<Integer> testValues;
	

	public void start(ArrayList<Integer> testValues) {
		this.testValues=testValues;
		if(Const.CHOICE.equals(Const.CHOICES.GetNodeByProperty)){
			queryGetNodeByProperty();

		}else if(Const.CHOICE.equals(Const.CHOICES.GetNodeByPropertyRange)){
			queryGetNodeByPropertyRange();

		}else if(Const.CHOICE.equals(Const.CHOICES.GetNodeByPropertyRangeGroupBy)){
			queryGetNodeByPropertyRangeGroupBy();
			
		}else if(Const.CHOICE.equals(Const.CHOICES.GetEdgeByProperty)){
			queryGetEdgeByProperty();

			
		}else if(Const.CHOICE.equals(Const.CHOICES.GetEdgeByPropertyRange)){
			queryGetEdgeByPropertyRange();
			
		}else if(Const.CHOICE.equals(Const.CHOICES.GetEdgeByPropertyGroupBy)){
			queryGetEdgeByPropertyGroupBy();

			
		}else if(Const.CHOICE.equals(Const.CHOICES.GetNeighborhood)){
			queryGetNeighborhood();
			
		}else if(Const.CHOICE.equals(Const.CHOICES.GetNeighborhoodFilter)){
			queryGetNeighborhoodFilter();

			
		}else if(Const.CHOICE.equals(Const.CHOICES.GetKhop)){
			queryKHop();
			
		}else if(Const.CHOICE.equals(Const.CHOICES.GraphMatchingConditionnal)){
			queryGraphMatchingConditionnal();

		}else if(Const.CHOICE.equals(Const.CHOICES.PageRank)){
			queryPageRank();

		}else if(Const.CHOICE.equals(Const.CHOICES.ShortestPath)){
			System.out.println("Shortes Path sans weight");

			queryShortestPath();
		}else if(Const.CHOICE.equals(Const.CHOICES.ShortestPathWeight)){
			System.out.println("Shortes Path avec weight");

			queryShortestPathWeight();
			
		}else if(Const.CHOICE.equals(Const.CHOICES.AddNode)){
			System.out.println("Add Node");
			queryAddNode();
			
		}else if(Const.CHOICE.equals(Const.CHOICES.DeleteNode)){
			System.out.println("Delete Node");

			queryDeleteNode();

		}else if(Const.CHOICE.equals(Const.CHOICES.AddEdge)){
			System.out.println("Add Edge");

			queryAddEdge();
		}else if(Const.CHOICE.equals(Const.CHOICES.DeleteEdge)){
			System.out.println("Delete Edge");

			queryDeleteEdge();

		}else if(Const.CHOICE.equals(Const.CHOICES.UpdateNode)){
			System.out.println("Update Node");

			queryUpdateProperty();

		}else if(Const.CHOICE.equals(Const.CHOICES.UpdateEdge)){
			System.out.println("Update Edge");

			queryUpdatePropertyWeight();

		}


	}



	
	protected abstract void queryGetNodeByProperty();

	protected abstract void queryGetNodeByPropertyRange();


	protected abstract void queryGetNodeByPropertyRangeGroupBy();

	
	protected abstract void queryGetEdgeByProperty();
	
	protected abstract void queryGetEdgeByPropertyRange();
	
	protected abstract void queryGetEdgeByPropertyGroupBy();

	protected abstract void queryGetNeighborhood();
	
	protected abstract void queryGetNeighborhoodFilter();
	
	protected abstract void queryKHop();
		
	protected abstract void queryGraphMatchingConditionnal();

	protected abstract void queryPageRank();


	protected abstract void queryShortestPath();
	protected abstract void queryShortestPathWeight();

	protected abstract void queryAddNode();
	
	protected abstract void queryDeleteNode();

	protected abstract void queryAddEdge();
	
	protected abstract void queryDeleteEdge();

	protected abstract void queryUpdateProperty();
	protected abstract void queryUpdatePropertyWeight();



}
