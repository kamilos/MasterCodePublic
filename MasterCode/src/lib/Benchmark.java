package lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Benchmark {
	
	protected Map<String, String> edges;
	protected Map<Integer, String> nodes;
	protected Map<Integer, ArrayList<Integer>> nodesEdges = new HashMap<>();
	protected ArrayList<Integer> weight;
	protected ArrayList<Integer> age;
	protected ArrayList<Integer> testValues;
	
	public void setData(Map<Integer, String> nodes, Map<String, String> edges) {
		this.nodes = nodes;
		this.edges = edges;
	}
	
	public void setWeight(ArrayList<Integer> weight) {
		this.weight = weight;
	}
	public void setAge(ArrayList<Integer> age) {
		this.age= age;
	}
	
	public void setData2(Map<Integer, ArrayList<Integer>> nodesEdges) {
		this.nodesEdges = nodesEdges;
	}
	
	public void setTestValues(ArrayList<Integer>  testValues) {
		this.testValues = testValues;
	}

	public void init(){
		
	}

}
