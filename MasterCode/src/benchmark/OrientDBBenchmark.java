package benchmark;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.orientechnologies.orient.core.intent.OIntentMassiveInsert;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.tinkerpop.blueprints.impls.orient.OrientEdgeType;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;
import com.tinkerpop.blueprints.impls.orient.OrientGraphNoTx;
import com.tinkerpop.blueprints.impls.orient.OrientVertex;
import com.tinkerpop.blueprints.impls.orient.OrientVertexType;

import benchmark.query.OrientDBQuery;
import constant.Const;
import lib.Benchmark;
import utils.Util;


//Note : The elements will be stored on heap until they are committed so a sufficiently large transaction will cause eventual heap exhaustion

public class OrientDBBenchmark extends Benchmark {

	// Tajouter au vm arguments pour massive insertion :
	//  -Dstorage.diskCache.bufferSize=20000
	// -Xmx1024M -Xms1024M and and allow disk cache to use the rest of RAM by
	// using setting storage.diskCache.bufferSize (in megabytes) and of course
	// do not go into swap.

	// BulkLoaderVertexProgram seems to be oriented towards graph-to-graph conversion.

	OrientGraphNoTx graphDb;

	private Map<Integer, OrientVertex> temp = new HashMap<>();

	public void init() {
		if (Const.INIT) {
			Util.deleteOldDB(new File(Const.DB_PATH_OrientDB));
		}

		OrientGraphFactory factory = new OrientGraphFactory("plocal:" + Const.DB_PATH_OrientDB, "admin", "admin");

		if (Const.INIT) {
			// retrieve a Transactional instance
			graphDb = factory.getNoTx();

			createClass("Person");
			createEdge("likes");
			createEdge("hates");
			
			OrientEdgeType type = graphDb.getEdgeType("E");
			//Note: cet index est pris en compte  (testé en faisant les UNIQUE )
			type.createProperty("weight", OType.INTEGER).createIndex("NOTUNIQUE_HASH_INDEX"); 
			type.createIndex("weight2", OClass.INDEX_TYPE.NOTUNIQUE, "weight");
			
			
			// Creation index, pour faciliter la recherche, le faire après le bulk ralentit
			graphDb.commit();
			factory.declareIntent(new OIntentMassiveInsert()); // on lui dit qu'on va faire un massive insertion
			graphDb = factory.getNoTx();
			graphDb.getRawGraph().setRetainRecords(false);
			graphDb.setUseLog(false);

			long time1 = System.nanoTime();
			bulkPopulate2();
			long time2 = System.nanoTime();
			System.out.println("Temps écoule pour charger les données : "+(time2-time1)/Math.pow(10, 9));


			graphDb.shutdown(); // return the Graph instance to the pool,

		}else{
			OrientGraph graphDb2 = factory.getTx();

			OrientDBQuery query = new OrientDBQuery(graphDb2);
			
			// Avoir les indexs
//			 for(OIndex<?> index : (graphDb2.getRawGraph().getMetadata().getIndexManager().getIndexes())){
//				 System.out.println(index.getName());
//			 }
			query.start(testValues);

			
		}
		factory.close(); // release all the instances and free all the resources


	}



	private void bulkPopulate2() {

		OrientVertex node1;
		OrientVertex node2;

		int counter = 0;
		int batchCommit = 5000;
		Iterator<Integer> ageIterator = age.iterator();

		for (Integer nodeId : nodesEdges.keySet()) {

			node1 = graphDb.addVertex("class:Person", "name", nodeId,"age",ageIterator.next());
			temp.put(nodeId, node1);

			counter++;
			//if (counter % batchCommit == 0) {
				graphDb.commit();
				//graphDb.getRawGraph().commit(); // nécessaire pour de grands

			//}
			//System.out.println("Noeuds Chargé"+counter);

		}
		counter = 0;
		System.out.println("Noeuds Chargés");
		
		Iterator<Entry<Integer, ArrayList<Integer>>> mapIterator = nodesEdges.entrySet().iterator();
		Iterator<Integer> values;
		Entry<Integer, ArrayList<Integer>> mapEntry;
		Iterator<Integer> weightIterator = weight.iterator();
		boolean first = true;
		while (mapIterator.hasNext()) {
			mapEntry = mapIterator.next();
			node1 = temp.get(mapEntry.getKey()); 
			values = mapEntry.getValue().iterator();
			while (values.hasNext()) {
				node2 = temp.get(values.next()); 


				if(first)
					node1.addEdge("likes", node2, null, null, "weight", weightIterator.next());
				else
					node1.addEdge("hates", node2, null, null, "weight", weightIterator.next());

				first=!first;
				// note: faut le faire dans ce sens la, null car orient ignore l'id car orientdb utilise un unique id

				counter++;
				//if (counter % batchCommit == 0) {
					graphDb.commit(); // nécessaire pour de grands

				//}
			}
			//System.out.println("Arcs chargés" + counter);
		}

		graphDb.commit();
	}
	
	

	void createEdge(final String edgeName) {

		graphDb.createEdgeType(edgeName);


	}

	void createClass(final String className) {

		graphDb.createVertexType(className);

		OrientVertexType accountVertex = graphDb.getVertexType(className);
		accountVertex.createProperty("name", OType.INTEGER).createIndex("UNIQUE_HASH_INDEX"); //pour accès direct
		accountVertex.createProperty("age", OType.INTEGER).createIndex("NOTUNIQUE"); //pour les range
		accountVertex.createProperty("pageRank", OType.DOUBLE).createIndex("NOTUNIQUE_HASH_INDEX"); 
		accountVertex.createIndex("age2", OClass.INDEX_TYPE.NOTUNIQUE_HASH_INDEX, "age");
		
		accountVertex.createProperty("outNodes", OType.INTEGER).createIndex("NOTUNIQUE_HASH_INDEX"); 




	}



	public void shutdown() {
	}

	
}
