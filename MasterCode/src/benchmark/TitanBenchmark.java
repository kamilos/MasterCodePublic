package benchmark;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.configuration.BaseConfiguration;
import org.apache.tinkerpop.gremlin.process.traversal.Order;
import org.apache.tinkerpop.gremlin.structure.Direction;
import org.apache.tinkerpop.gremlin.structure.T;
import org.apache.tinkerpop.gremlin.structure.Vertex;

import com.thinkaurelius.titan.core.Cardinality;
import com.thinkaurelius.titan.core.EdgeLabel;
import com.thinkaurelius.titan.core.Multiplicity;
import com.thinkaurelius.titan.core.PropertyKey;
import com.thinkaurelius.titan.core.TitanFactory;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.TitanVertex;
import com.thinkaurelius.titan.core.VertexLabel;
import com.thinkaurelius.titan.core.schema.TitanManagement;

import benchmark.query.TitanQueryNew;
import constant.Const;
import lib.Benchmark;
import utils.Util;

public class TitanBenchmark extends Benchmark {

	private TitanGraph graphDb;
	private Map<Integer, TitanVertex> temp = new HashMap<>();

	public void init() {

		//Titan's standard index only supports equality comparisons (i.e. no range queries).
		BaseConfiguration conf = new BaseConfiguration();
		conf.setProperty("storage.backend", "berkeleyje");
		conf.setProperty("storage.directory", Const.DB_PATH_Titan);
		conf.setProperty("ids.block-size", nodesEdges.keySet().size());
		conf.setProperty("schema.default", "none");
		
		//Range queries via Lucene
		conf.setProperty("index.search.backend", "lucene"); //the index backend is named search
		conf.setProperty("index.search.directory",Const.DB_PATH_Titan_LUCENE_INDEX);
		
				
		if (Const.INIT) {
			Util.deleteOldDB(new File(Const.DB_PATH_Titan));
			Util.deleteOldDB(new File(Const.DB_PATH_Titan_LUCENE_INDEX));

			

			// biggest positive impact on bulk loading times, Titan assumes that the data to be loaded into Titan is consistent
			// with the graph and hence disables its own checks in the interest of performance.
			conf.setProperty("storage.batch-loading", true);
			conf.setProperty("storage.transactions", false);
			conf.setProperty("storage.berkeleydb.cache-percentage", 90);

			
			graphDb = TitanFactory.open(conf);

			// populate(conf);
			populate2(conf);
			testIndex();

		}else{
			conf.setProperty("storage.transactions", true);
			conf.setProperty("storage.batch-loading", false);

			graphDb = TitanFactory.open(conf);
			TitanQueryNew queries = new TitanQueryNew(graphDb);
			queries.start(testValues);

		}
		graphDb.close();


		

	}

	private void testIndex() {
//		for(PropertyKey i : graphDb.openManagement().getGraphIndex("name").getFieldKeys() ){
//			System.out.println(i.name());
//		}
//		for(PropertyKey i : graphDb.openManagement().getGraphIndex("ageLucene").getFieldKeys() ){
//			System.out.println(i.name());
//		}


	}



	public void populate2(BaseConfiguration conf) {

		graphDb = TitanFactory.open(conf);
		TitanManagement mgmt = graphDb.openManagement();

		// Creation de labels sur arcs
		EdgeLabel likes = mgmt.makeEdgeLabel("likes").multiplicity(Multiplicity.MULTI).make(); // mutli peut y avoir + eurs knows entre 2 vertices
		EdgeLabel hates = mgmt.makeEdgeLabel("hates").multiplicity(Multiplicity.MULTI).make();
		
		// Creation de label
		VertexLabel person = mgmt.makeVertexLabel("Person").make();

		// Creation de propriété
		PropertyKey name = mgmt.makePropertyKey("name").dataType(Integer.class).cardinality(Cardinality.SINGLE).make(); //single = 1 valeur par noeud
		PropertyKey ageProperty = mgmt.makePropertyKey("age").dataType(Integer.class).cardinality(Cardinality.SINGLE).make();
		PropertyKey weightProperty = mgmt.makePropertyKey("weight").dataType(Integer.class).cardinality(Cardinality.SINGLE).make();
		PropertyKey pageRankProperty = mgmt.makePropertyKey("pageRank").dataType(Double.class).cardinality(Cardinality.SINGLE).make();
		
		PropertyKey outNodesProperty = mgmt.makePropertyKey("outNodes").dataType(Integer.class).cardinality(Cardinality.SINGLE).make();


		// Creation d'index
		// index + unique + portant sur propriété du label
		mgmt.buildIndex("name", Vertex.class).addKey(name).unique().buildCompositeIndex();
		mgmt.buildIndex("age", Vertex.class).addKey(ageProperty).buildCompositeIndex();
		mgmt.buildIndex("pageRank", Vertex.class).addKey(pageRankProperty).buildCompositeIndex();
		mgmt.buildIndex("outNodes", Vertex.class).addKey(outNodesProperty).buildCompositeIndex();


		mgmt.buildIndex("ageLucene", Vertex.class).addKey(ageProperty).buildMixedIndex("search"); //extarnal index cannot be unique ?
		mgmt.buildEdgeIndex(likes, "weight", Direction.OUT, Order.decr, weightProperty); 
		mgmt.buildEdgeIndex(hates, "weight", Direction.OUT, Order.decr, weightProperty); 
		mgmt.buildEdgeIndex(likes, "weightIncr", Direction.OUT, Order.incr, weightProperty); 
		mgmt.buildEdgeIndex(hates, "weightInc", Direction.OUT, Order.incr, weightProperty); 

		mgmt.commit();


		TitanVertex node1;
		TitanVertex node2;

		int i = 0;
		Iterator<Integer> ageIterator = age.iterator(); //All iterators have as a fundamental requirement that next() should be an O(1) operation,

		for (Integer nodeID : nodesEdges.keySet()) {

			//Note: on peut associer propre id avec T.id
			node1 = graphDb.addVertex(T.label,"Person","name",nodeID,"age",ageIterator.next());
			temp.put(nodeID, node1);
			i++;
			if (i % 10000 == 0) {
				graphDb.tx().commit(); // commit transaction et démarre une nouvelle
			}
		}
		i = 0;

		Iterator<Entry<Integer, ArrayList<Integer>>> mapIterator = nodesEdges.entrySet().iterator();
		Iterator<Integer> values;
		Entry<Integer, ArrayList<Integer>> mapEntry;
		Iterator<Integer> weightIterator = weight.iterator();
		boolean first = true;

		while (mapIterator.hasNext()) {
			mapEntry = mapIterator.next();
			node1 = temp.get(mapEntry.getKey()); // getOrCreateNode(mapEntry.getKey());
			values = mapEntry.getValue().iterator();
			while (values.hasNext()) {

				node2 = temp.get(values.next()); // getOrCreateNode(values.next());

				if(first){
					node1.addEdge("likes", node2).property("weight", weightIterator.next());
				}else{
					node1.addEdge("hates", node2).property("weight", weightIterator.next());
				}
				first = !first;

				i++;
				if (i % 10000 == 0) { //S'il n'y en a pas, ca marche pas
					graphDb.tx().commit();
				}

			}
		}

		graphDb.tx().commit(); // ferme la transation

	}




}
