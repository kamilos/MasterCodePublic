package benchmark;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.schema.ConstraintDefinition;
import org.neo4j.graphdb.schema.IndexDefinition;
import org.neo4j.unsafe.batchinsert.BatchInserter;
import org.neo4j.unsafe.batchinsert.BatchInserters;

import benchmark.query.Neo4JCypherQuery;
import benchmark.query.Neo4JQuery;
import constant.Const;
import lib.Benchmark;
import utils.Util;

public class Neo4JBenchmark extends Benchmark {

	GraphDatabaseService graphDb;

	// BATCH inserter part
	private Map<String, Object> properties = new HashMap<>();
	private Map<String, Object> weightProperty = new HashMap<>();

	private Map<Integer, Long> temp = new HashMap<>();
	private BatchInserter inserter = null;




	public static enum RelTypes implements RelationshipType {
		Likes, Hates
	}

	public static Label labelPerson = DynamicLabel.label("Person");
	public static String propertyWeight = "weight";
	public static String propertyName = "name";
	public static String propertyAge = "age";
	public static String propertyPageRank = "pageRank";
	public static String propertyoutNode = "outNode";

	public static String propertyCost = "cost";
	public static String propertyPredecessor = "predecessor";
	public static String propertyCalculated = "calculated";

	

	public void init() {

		if (Const.INIT) {
			buildDb();
		}else{
			graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(new File(Const.DB_PATH_Neo4J));
			registerShutdownHook(graphDb);
			
			//showIndex();

			// Execution des queries
			Neo4JQuery queries = new Neo4JQuery(graphDb);
			queries.start(testValues);
			graphDb.shutdown();
		}
	}


	private void showIndex() {
		System.out.println("index");
		try (Transaction tx = graphDb.beginTx()) {

			Iterator<ConstraintDefinition> constraints = graphDb.schema().getConstraints(Neo4JBenchmark.labelPerson)
					.iterator();
			while (constraints.hasNext()) {
				System.out.println(constraints.next());
			}
			Iterator<IndexDefinition> index = graphDb.schema().getIndexes().iterator();
			while (index.hasNext()) {
				System.out.println(index.next());
			}
			System.out.println("--");

		}
				
	}


	private void buildDb() {
		Util.deleteOldDB(new File(Const.DB_PATH_Neo4J));
		bulkPopulate2();
		inserter.shutdown();		
	}


	private void bulkPopulate2() {
		
		//  bytes_needed = number_of_nodes * 15 + number_of_relationships *
		// 34 + number_of_properties * 64
		// Map<String, String> config = new HashMap<>();
		// config.put( "dbms.pagecache.memory", "4g" );

		try {
			inserter = BatchInserters.inserter(new File(Const.DB_PATH_Neo4J));
		} catch (IOException e) {
			e.printStackTrace();
		}

		Label personLabel = DynamicLabel.label("Person");
		inserter.createDeferredConstraint(personLabel).assertPropertyIsUnique(propertyName).create(); //crée unique + index
		inserter.createDeferredSchemaIndex(personLabel).on(propertyAge).create();
		inserter.createDeferredSchemaIndex(personLabel).on(propertyPageRank).create();
		inserter.createDeferredSchemaIndex(personLabel).on(propertyoutNode).create();

		long node1;
		long node2;
		Iterator<Integer> ageIterator = age.iterator();
		for (Integer id : nodesEdges.keySet()) {
			properties.put(propertyName, id); // replace avec la valeur avant
			properties.put(propertyAge, ageIterator.next()); // replace avec la valeur avant
			long nodeId = inserter.createNode(properties, labelPerson);
			temp.put(id, nodeId);
		}


		Iterator<Entry<Integer, ArrayList<Integer>>> mapIterator = nodesEdges.entrySet().iterator();
		Iterator<Integer> values;
		Entry<Integer, ArrayList<Integer>> mapEntry;
		Iterator<Integer> weightIterator = weight.iterator();
		boolean first = true;
		while (mapIterator.hasNext()) {
			mapEntry = mapIterator.next();
			node1 = temp.get(mapEntry.getKey());
			values = mapEntry.getValue().iterator();
			while (values.hasNext()) {
				weightProperty.put(propertyWeight, weightIterator.next());
				node2 = temp.get(values.next());
				if(first)
				 inserter.createRelationship(node1, node2, RelTypes.Likes, weightProperty);
				else
					inserter.createRelationship(node1, node2, RelTypes.Hates, weightProperty);
				first = !first;


			}
		}


	}



	

	private static void registerShutdownHook(final GraphDatabaseService graphDb) {
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDb.shutdown();
			}
		});
	}

	public void setData2(Map<Integer, ArrayList<Integer>> nodesEdges) {
		this.nodesEdges = nodesEdges;

	}


	public void initCypherMode() {
		if (Const.INIT) {
			buildDb();
			
		}else{
			
			graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(new File(Const.DB_PATH_Neo4J));
			registerShutdownHook(graphDb);
			//showIndex();
			
			Neo4JCypherQuery queries = new Neo4JCypherQuery(graphDb);
			queries.start(testValues);
			graphDb.shutdown();


		}	

	}


	public void shutdown() {
		
	}

}
