package benchmark;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import benchmark.query.MysqlQuery;
import constant.Const;
import lib.Benchmark;


public class MysqlBenchmark extends Benchmark {


	private Connection connection;

	public void init() {

		

		if (Const.INIT) {
			try {
				connection = DriverManager.getConnection(Const.SQL_ADRESS_ROOT, "root", "");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			deleteOldDb();
			long time1 = System.nanoTime();
			
			try {
				bulkPopulate2();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			long time2 = System.nanoTime();
			System.out.println("Chargement terminé : temps écoule"+(time2-time1)/Math.pow(10, 9));
		} else {
			try {
				connection = DriverManager.getConnection(Const.SQL_ADRESS, "root", "");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			// Execution des queries
			MysqlQuery queries = new MysqlQuery();
			queries.setConnection(connection);
			queries.start(testValues);

		}
		close();

	}


	private void deleteOldDb() {
		executeUpdate("DROP DATABASE IF EXISTS "+Const.DB_PATH_MYSQL);
	}



	private void executeUpdate(String sql) {
		try {
			connection.prepareStatement(sql).executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void bulkPopulate2() throws SQLException {
		try {
			createTable();
		} catch (Exception e) {
			e.printStackTrace();
		}
		connection.close();
		connection = DriverManager.getConnection(Const.SQL_ADRESS_BATCH, "root", "");

		
		java.sql.Statement stmt = connection.createStatement();
		stmt.execute("SET FOREIGN_KEY_CHECKS=0"); //Si pas global alors valable uniquement pour cette session
		stmt.execute("SET unique_checks=0;");
		stmt.execute("SET GLOBAL max_allowed_packet=1073741824;"); //peut pas aller plus loin
		  
		stmt.close();
		
		connection.setAutoCommit(false);

		long node1;
		long node2;
		PreparedStatement preparedStatement = null;
		preparedStatement = connection.prepareStatement("insert into node values (?, ?, default, default)"); //username, age

		int i = 0;
		Iterator<Integer> ageIterator = age.iterator();
		for (Integer node : nodesEdges.keySet()) {
			preparedStatement.setInt(1, node);
			preparedStatement.setInt(2, ageIterator.next());
			preparedStatement.addBatch();
			if (i % 1000 == 0) { // Execute every 1000 items.
				preparedStatement.executeBatch();
			}
			i++;
		}
		preparedStatement.executeBatch();

		i = 0;
		preparedStatement = connection.prepareStatement("insert into edge values (?, ?, ?, ?)");

		Iterator<Entry<Integer, ArrayList<Integer>>> nodeIterator = nodesEdges.entrySet().iterator();
		Iterator<Integer> values;
		Entry<Integer, ArrayList<Integer>> mapEntry;
		Iterator<Integer> weightIterator = weight.iterator();
		boolean first = true;
		while (nodeIterator.hasNext()) {
			mapEntry = nodeIterator.next();
			node1 = mapEntry.getKey();
			values = mapEntry.getValue().iterator();
			while (values.hasNext()) {
				// weightProperty.put(propertyWeight, );
				node2 = values.next();
				preparedStatement.setInt(1, (int) node1); 
				preparedStatement.setInt(2, (int) node2);
				preparedStatement.setInt(3, weightIterator.next());
				if(first)
					preparedStatement.setString(4, "likes");
				else
					preparedStatement.setString(4, "hates");
				first = !first;

				preparedStatement.addBatch();
				if (i % 1000==0) { // Execute every 1000 items.
					preparedStatement.executeBatch();
					//connection.commit();
				}
				i++;
			}
		}
		preparedStatement.executeBatch();
		connection.commit();

	}

	private void createTable() throws SQLException, FileNotFoundException {

		Scanner scanner = new Scanner(new FileInputStream(Const.SQL_TABLES_PATH), "UTF-8");
		String contents = scanner.useDelimiter("\\Z").next();
		scanner.close();

		
		// Création de la DB
		String[] sql = contents.split(";");
		connection.prepareStatement(sql[0].trim().replace("masterDB", Const.DB_PATH_MYSQL)).executeUpdate(); 
		connection.close();
		

		// Création à la DB // NOTE : PAS D ESPACE DANS LE FICHIER SQL !!
		connection = DriverManager.getConnection(Const.SQL_ADRESS, "root", "");
		for (int i = 1; i < sql.length; ++i) {
			connection.prepareStatement(sql[i].trim()).executeUpdate(); 

		}
		
		System.out.println("fin creation de tables");

	}

	public void close() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void setData2(Map<Integer, ArrayList<Integer>> nodesEdges) {
		this.nodesEdges = nodesEdges;

	}

}
