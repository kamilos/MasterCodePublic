package benchmark.query;




import static org.apache.tinkerpop.gremlin.process.traversal.P.neq;
import static org.apache.tinkerpop.gremlin.process.traversal.P.not;
import static org.apache.tinkerpop.gremlin.process.traversal.P.within;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.apache.tinkerpop.gremlin.structure.Direction;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.T;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.structure.VertexProperty;

import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.TitanVertex;
import com.tinkerpop.pipes.util.structures.Table;

import benchmark.query.shortestPath.TitanShortestPath;
import lib.Query;


public class TitanQueryNew extends Query {

	protected TitanGraph graphDb;
	protected long time1;
	protected long time2;
	protected GraphTraversalSource g;

	private long time11;

	public TitanQueryNew(TitanGraph graphDb) {
		this.graphDb = graphDb;
		g = graphDb.traversal();
		
//		System.out.println("Nodes : " + Iterables.size(graphDb.query().vertices()));
//		System.out.println("Edges : " + Iterables.size(graphDb.query().edges()));
//		

//		printGraph();

	}

	public void start(ArrayList<Integer> testValues) {
		super.start(testValues);
			
	}

	protected void queryGraphMatchingConditionnal() {
		int cpt=0;
		Table t = new Table();
		List<Map<String, Object>> list ;
		time1 = System.nanoTime();
		for(int i = 0;i<testValues.size();i++){
			list = g.V().has("name",testValues.get(i)).as("a").outE("likes").has("weight", P.lt(400)).inV().where(neq("a")).has("age",P.gt(18)).as("b")
						.out("hates").where(neq("a").and(neq("b"))).has("age",P.gt(18)).as("c")
						.where(__.inE("likes").has("weight", P.lt(400)).outV().as("a"))
						.select("a","b","c").toList();//by("name").limit(1)
			cpt+=list.size();
			list.clear();
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	protected void queryGetEdgeByPropertyGroupBy() {
		
		int cpt=0;

		List<Map<Object, Long>> a;
		time1 = System.nanoTime();
		for(int i = 1;i<testValues.size();i++){ //avant 10 000 avec limit 10
			a = g.E().has("weight",P.gte(testValues.get(i))).has("weight", P.lte(testValues.get(i)+50)).groupCount().by("age").toList();
			cpt+=a.size();
		}		
		
				
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println("cpt"+cpt);
	}


	protected void queryKHop() {
	
		time1 = System.nanoTime();
		int cpt=0;
		List<Vertex> a;
		List<Integer> b = new ArrayList<Integer>();

		int res = 0;
		for(int i = 1;i<testValues.size();i++){ //avant 10 000 avec limit 10
			

			a = (List<Vertex>) g.V().has("name",testValues.get(i)).aggregate("source")
								 .out("likes").where(not(within("source"))).aggregate("nodesDist1")
								 .out("likes").dedup().where(not(within("nodesDist1")).and((not(within("source"))))).aggregate("nodesDist2")
								 .out("likes").dedup().where(not(within("nodesDist1")).and((not(within("source")))).and(not(within("nodesDist2"))))
								 .limit(50).toList(); 

			cpt+=a.size();
			a.clear();			
		}
		
		
			
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println("cpt"+cpt);

	}

	protected void queryGetNeighborhoodFilter() {
		Iterator<Vertex> it = null;
	
		
		time1 = System.nanoTime();
		int cpt=0;
		Iterator<Vertex> temp;
		for(int i = 1;i<testValues.size();i++){
			
			it=  g.V().has("name", testValues.get(i)).outE("likes").has("weight", P.gte(200)).inV();
				
			if(it!=null){
			 while(it.hasNext()){
				 	it.next();
			 		cpt++;
			    }	
			}
			
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println("cpt"+cpt);

	}

	protected void queryGetEdgeByProperty() {
		Vertex res;
		Iterator<Edge> it = null ;


		int cpt=0;
		
		time1 = System.nanoTime();

		for(int i = 1;i<testValues.size();i++){
				it = g.E().has("weight", testValues.get(i)).limit(100);  //N'y ARRIVE PAS !

		
			while (it.hasNext()) {
				it.next();
				cpt++;
			}
			
		}


		System.out.println(cpt);
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
	}



	protected void queryGetNodeByPropertyRangeGroupBy() {
		Vertex res;
		Iterator<Vertex> it = null ;


		int cpt=0;
		List<Map<Object, Long>> a;
		time1 = System.nanoTime();
		
		for(int i = 1;i<testValues.size();i++){
				a = g.V().has("age",P.gte(testValues.get(i))).has("age", P.lte(testValues.get(i)+10))
						 .groupCount().by("age").toList();
				a.clear();
				cpt+=a.size();
		}

		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt); //616269

	}

	protected void queryGetNodeById() {
		
		
		Iterator<Vertex> it = graphDb.vertices(4176);
		if (it.hasNext()) {
			System.out.println(it.next().value("name").toString());
		}
	}

	protected void printGraph() {
		
		//Note: le count a pour effet de prendre tt les vertices et les mettre en ram
		Iterator<Vertex> verticesIt = graphDb.vertices();
		while(verticesIt.hasNext()){
			Vertex v =verticesIt.next();
			System.out.print(v.id() + ":");
			System.out.print(v.label() + ":");
			Iterator<VertexProperty<Object>> prop = v.properties();
			while(prop.hasNext())
				System.out.print(prop.next());
			System.out.println();
		}
		
		System.out.println();
		
		Iterator<Edge> itEdges = graphDb.edges();
		Edge edge;
		while(itEdges.hasNext()){
			edge = itEdges.next();
			Vertex n2 = edge.inVertex();
			Vertex n1 = edge.outVertex();
			System.out.print(n1.property("name") + " --> " + n2.property("name") + " (" + edge.label() + ") ");
				System.out.print("weight" + "=" + edge.property("weight") + ", ");
				System.out.print(" id = " + edge.id());
			System.out.println();

		}	
	}


	protected void queryGetEdgeByPropertyRange() {

		Iterator<Edge> it = null ;


		int cpt=0;
		
		time1 = System.nanoTime();

		for(int i = 1;i<testValues.size();i++){
				
			it = g.E().has("weight", P.gte(testValues.get(i))).has("weight", P.lte(testValues.get(i)+50)).limit(1000);//.limit(5);
			
			while (it.hasNext()) {
				it.next();
				cpt++;
			}
			
		}
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));		
		System.out.println(cpt);		


	}

	protected void queryGetNodeByPropertyRange() {
		
		Iterator<Vertex> it = null ;

		
		int cpt=0;
		time1 = System.nanoTime();
		

		for(int i = 1;i<testValues.size();i++){
			
			it = g.V().has("age", P.gte(testValues.get(i))).has("age", P.lte(testValues.get(i)+10)).limit(500);
			
			while (it.hasNext()) {
				it.next();
				cpt++;
			}
		}
		
				
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);

		
		

	}

	protected void queryPageRank() {


		double dampingfactor = 0.85;

		double prSumValue;
		Iterator<Edge> edges;
		Vertex tempV;
		Iterator<Edge> tempE;
		int links;
		double leftPart = ((1.0 - dampingfactor)); // dans autres version divise par nv
		int iter = 4 ;//testValues.size() ;
		
	      time11 = System.nanoTime();

		long nv = g.V().count().next();


		time2=System.nanoTime();
	      System.out.println("Comptage Temps écoule : "+(time2-time11)/Math.pow(10, 9));
	     
	      time1 = System.nanoTime();
		double initValue = 1.0 / nv;
		for (Vertex v : graphDb.query().vertices()) {
			v.property("pageRank", initValue);
			tempE = v.edges(Direction.OUT);

			links =0;
			while(tempE.hasNext()){ 
				tempE.next();
				links++;
			};
			v.property("outNodes", links);

		}


		while (iter > 0) {
		      
			for (Vertex v : graphDb.query().vertices() ) {
				prSumValue = 0;
				edges = v.edges(Direction.IN); //Note: tempVIt = g.V(v.id()).in(); est moins rapide
				while (edges.hasNext()) {									
					tempV = edges.next().outVertex(); 
					prSumValue += (Double) tempV.property("pageRank").value() / (int)tempV.property("outNodes").value();	

				}
	
	    		v.property("pageRank", leftPart + (dampingfactor * prSumValue));

			}
			
			iter--;

		}
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time11)/Math.pow(10, 9));


	}

	protected void queryShortestPath() {

		
		Iterator<Vertex> vertex1it;
		Iterator<Vertex> vertex2it;
		int cpt =0;
		time1 = System.nanoTime();
		for(int i = 0;i<testValues.size()-1;i++){
			vertex1it = g.V().has("name", testValues.get(i));
			vertex2it = g.V().has("name", testValues.get(i+1));
			

			if(vertex1it.hasNext() && vertex2it.hasNext() ){
				TitanShortestPath shortest = new TitanShortestPath(graphDb,(int)vertex1it.next().property("name").value() ,(int)vertex2it.next().property("name").value());
				shortest.modeComplete(false);
				shortest.start();
				
			
				if(shortest.getDistance()!=-1){
					cpt+=shortest.getDistance();
				}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
		
	


	}



	protected void queryDeleteNode() {

		

		ArrayList<Vertex> toDelete1 =new ArrayList<Vertex>();
		for(int i = 1;i<=testValues.size();i++){
			toDelete1.add(g.V().has("name", -i).next());	
		}
		
		Iterator<Vertex> toDeleteIt = toDelete1.iterator();
		int i=0;
		time1=System.nanoTime();
		while(toDeleteIt.hasNext()){
			toDeleteIt.next().remove();
			i++;
			if(i%4000==0){
				graphDb.tx().commit();
			}
		}		
		graphDb.tx().commit();
		time2=System.nanoTime();
		double average = ((time2-time1)/Math.pow(10, 9));

		System.out.println(average);

	}

	protected void queryDeleteEdge() {
		
		ArrayList<Edge> toDelete1 =new ArrayList<Edge>();

		for(int i = 1;i<testValues.size();i++){
			toDelete1.add(g.V().has("name", -i).next().edges(Direction.OUT).next());
		}
		Iterator<Edge> toDeleteIt = toDelete1.iterator();
		int i =0;
		time1=System.nanoTime();
		while(toDeleteIt.hasNext()){
			toDeleteIt.next().remove();
			i++;
			if(i%4000==0){
				graphDb.tx().commit();
			}
		}

		graphDb.tx().commit();
		time2=System.nanoTime();

		System.out.println((time2-time1)/Math.pow(10, 9));
	
	}

	protected void queryUpdateProperty() {
	
		ArrayList<Vertex> toDelete1 =new ArrayList<Vertex>();
		
		for(int i = 1;i<=testValues.size();i++){
				toDelete1.add( g.V().has("name", -i).next());
		}
		
		time1=System.nanoTime();
		for(int i = 0;i<testValues.size();i++){
			toDelete1.get(i).property("age", 18);
				if(i%4000==0){
					graphDb.tx().commit();
				}
		}
		graphDb.tx().commit();
		time2=System.nanoTime();
		
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		
	}



	protected void queryAddEdge() {
		
		ArrayList<Vertex> toDelete =new ArrayList<Vertex>();
		for(int i = 1;i<testValues.size();i++){	
				toDelete.add(g.V().has("name", -i).next());
				toDelete.add(g.V().has("name", -i-1).next());
		}
		Iterator<Vertex> toDeleteIt = toDelete.iterator();
		
		time1=System.nanoTime();
		int i =0;
		while(toDeleteIt.hasNext()){
			toDeleteIt.next().addEdge("likes", toDeleteIt.next(),"weight",-1);	
			i++;
			if(i%4000==0){
				graphDb.tx().commit();
			}
		}		
		graphDb.tx().commit();
		time2=System.nanoTime();
		double average = ((time2-time1)/Math.pow(10, 9));

		System.out.println(average);

	}

	protected void queryAddNode() {
		TitanVertex res;
	
		time1 = System.nanoTime();	
		for(int i = 1;i<=testValues.size();i++){
			
			graphDb.addVertex(T.label,"Person","name",-i,"age",-1);
			//graphDb.addVertex("name",-i,"age",-1); // 81 sec
			if(i%4000==0){
				graphDb.tx().commit();
			}

		}
		time2=System.nanoTime();
		graphDb.tx().commit();

		System.out.println("Temps écoule pour ajouter"+(time2-time1)/Math.pow(10, 9));
	}

	

	protected void queryGetNeighborhood() {

		Iterator<Vertex> it = null;
		time1 = System.nanoTime();
		int cpt=0;
		Iterator<Vertex> temp;
		//Apres plusieurs test, les deux se vallent
		for(int i = 1;i<testValues.size();i++){
			
			it=  g.V().has("name", testValues.get(i)).out(); // acces direct à la propriété est très lent
				
			if(it!=null){
			 while(it.hasNext()){
				 	it.next();
			 		cpt++;
			    }	
			}
			
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println("cpt"+cpt);
	}

	
	protected void queryGetNodeByProperty() {
		
		
		Vertex res;
		Iterator<Vertex> it = null ;

		
		int cpt=0;
		
		time1 = System.nanoTime();

		for(int i = 1;i<testValues.size();i++){
		
			it = g.V().has("name", testValues.get(i));	
			

			while (it.hasNext()) {
				it.next();
				cpt++;
			}
			
		}

		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);


	}
	
	

	@Override
	protected void queryUpdatePropertyWeight() {
		ArrayList<Edge> toDelete1 =new ArrayList<Edge>();

		for(int i = 1;i<testValues.size();i++){
			toDelete1.add(g.V().has("name", -i).next().edges(Direction.OUT).next());
		}
		Iterator<Edge> toDeleteIt = toDelete1.iterator();
		int i =0;
		time1=System.nanoTime();
		while(toDeleteIt.hasNext()){
			toDeleteIt.next().property("weight", -1);
			i++;
			if(i%4000==0){
				graphDb.tx().commit();
			}
		}

		graphDb.tx().commit();
		time2=System.nanoTime();

		System.out.println((time2-time1)/Math.pow(10, 9));		
	}

	@Override
	protected void queryShortestPathWeight() {
		Iterator<Vertex> vertex1it;
		Iterator<Vertex> vertex2it;
		int cpt =0;
		int dist;
		time1 = System.nanoTime();
		double time = 0;
		TitanShortestPath shortest;
		for(int i = 0;i<testValues.size()-1;i++){
			vertex1it = g.V().has("name", testValues.get(i));
			vertex2it = g.V().has("name", testValues.get(i+1));
			

			if(vertex1it.hasNext() && vertex2it.hasNext() ){
				 shortest = new TitanShortestPath(graphDb,(int)vertex1it.next().property("name").value() ,(int)vertex2it.next().property("name").value());
				shortest.modeComplete(true);
				shortest.start();
				

				dist = shortest.getDistance();
				if(dist!=-1){
					cpt+=dist;
				}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);	
	}


	
}
