package benchmark.query.shortestPath;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SQLShortestPath extends AbstractShortestPath<Integer, Integer,Connection > {
	protected PreparedStatement preparedStatement;
	protected Connection connection;
	List<Integer> neighbors;
	Integer temp;

	ResultSet res;
	String sql;


	public SQLShortestPath(Connection  connection, Integer start, Integer destination) {
		super(connection,start,destination);
		this.connection=connection;
	}


	@Override
	protected List<Integer> getNeighborsFrom(Integer node) {
		sql = "select n2.name from node n,edge e, node n2 where n.name=? and e.inNode = n.name  and e.outNode = n2.name";
		neighbors = new ArrayList<Integer>();
		try {

			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, node);
			res = preparedStatement.executeQuery();
			while(res.next()){
				temp = res.getInt(1);
				
				if (!seenNodes.contains(temp)) {
					neighbors.add(temp);
				}

			}
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return neighbors;				
	}


	@Override
	protected int getDistanceBetween(Integer node, Integer target) {
		if(complete){
			sql = "select e.weight from node n,edge e, node n2 where n.name=? and n2.name = ? and e.inNode = n.name and e.outNode = n2.name";
	
			try {
				
				preparedStatement = connection.prepareStatement(sql);
				preparedStatement.setInt(1, node);
				preparedStatement.setInt(2, target);
	
				res = preparedStatement.executeQuery();
				
				if(res.next()){
					temp = res.getInt(1);
				}
				
				preparedStatement.close();
	
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}else{
			temp = 1;
		}
		return temp;
	}
	
	@Override
	public void getPath() {
		ArrayList<Integer> path = new ArrayList<Integer>();
		Integer step = destinationNode;
	
		if (predecessors.get(step) == null) {
			System.out.println("rien");

		}
		path.add(step);
		while (predecessors.get(step) != null) {
			step = predecessors.get(step);
			path.add(step);
		}
	
		Collections.reverse(path);
		System.out.println(path.toString());
	}


}
