package benchmark.query.shortestPath;

import static org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__.__;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.Vertex;

import com.thinkaurelius.titan.core.TitanGraph;

public class TitanShortestPath extends AbstractShortestPath<Integer, Edge,TitanGraph> {

	public List<Integer> neighbors;
	Iterator<Edge> outEdge ;
	Integer temp ;
	Edge tempE ;

	private GraphTraversalSource g;
	private Iterator<Object> tempIt;
	private Vertex tempV;

	public TitanShortestPath(TitanGraph graphDb, Integer start, Integer destination) {
		super(graphDb,start,destination);
		g= graphDb.traversal();
	}

	@Override
	protected List<Integer> getNeighborsFrom(Integer node) {
		
		neighbors = new ArrayList<Integer>();  //plus rapide que clear
		//faire avec un query à l'interieur de V obitent même perf
		tempIt =g.V().has("name", node).out().properties("name").value();
		cpt++;

		while(tempIt.hasNext()){

			temp = (int)tempIt.next();
			if (!seenNodes.contains(temp)) {
				neighbors.add(temp);
			}
		}
		return neighbors;		
	
	}


	@Override
	protected int getDistanceBetween(Integer node, Integer target) {
		if(complete){
			tempV=g.V().has("name", target).next();
			return (int)g.V().has("name", node).outE().where(__().inV().is(tempV)).next().property("weight").value(); 
		}else{
			return 1;
		}
	}
	
	@Override
	public void getPath() {
		ArrayList<Integer> path = new ArrayList<Integer>();
		Integer step = destinationNode;
	
		if (predecessors.get(step) == null) {
			System.out.println("rien");

		}
		path.add((int)g.V().has("name",step).next().property("name").value());
		while (predecessors.get(step) != null) {
			step = predecessors.get(step);
			path.add((int)g.V().has("name",step).next().property("name").value());
		}
	
		Collections.reverse(path);
		System.out.println(path.toString());
	}


}
