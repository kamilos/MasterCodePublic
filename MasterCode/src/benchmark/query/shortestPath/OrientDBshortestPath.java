package benchmark.query.shortestPath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.impls.orient.OrientEdge;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.gremlin.java.GremlinPipeline;

public class OrientDBshortestPath extends AbstractShortestPath<Integer, OrientEdge,OrientGraph> {

	List<Integer> neighbors;
	Iterator<Edge> outEdge;
	Integer temp ;
	Edge tempE ;
	
	private Iterator<Integer>  tempIt;

	public OrientDBshortestPath(OrientGraph graphDb, Integer start,Integer destination) {
		super(graphDb,start,destination);
	}


	@Override
	protected List<Integer> getNeighborsFrom(Integer node) {

		neighbors = new ArrayList<Integer>();		
		tempIt= new GremlinPipeline(graphDb.getVertices("Person.name",node)).out().property("name");
		while(tempIt.hasNext()){
			temp = tempIt.next();
			if (!seenNodes.contains(temp)) {
				neighbors.add(temp);
			}
		}
		return neighbors;				
	}


	@Override
	protected int getDistanceBetween(Integer node, Integer target) {	
		if(complete){			
			return ((Edge) new GremlinPipeline(graphDb.getVertices("Person.name",node)).outE().inV().has("name", target).back(2).next()).getProperty("weight");
		}else{
			return 1;
		}
	}
	
	@Override
	public void getPath() {
		ArrayList<Integer> path = new ArrayList<Integer>();
		Integer step = destinationNode;
	
		if (predecessors.get(step) == null) {
			System.out.println("rien");

		}
		path.add((Integer)graphDb.getVertices("Person.name",step).iterator().next().getProperty("name"));
		while (predecessors.get(step) != null) {
			step = predecessors.get(step);
			path.add((Integer)graphDb.getVertices("Person.name",step).iterator().next().getProperty("name"));
		}
	
		Collections.reverse(path);
		System.out.println(path.toString());
	}
}
