package benchmark.query.shortestPath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import benchmark.Neo4JBenchmark;

public class Neo4jShortestPath extends AbstractShortestPath<Integer, Relationship,GraphDatabaseService> {
	List<Integer> neighbors;
	private Node tempV;
	

	public Neo4jShortestPath(GraphDatabaseService graphDb, Integer start, Integer destination) {
		super(graphDb,start,destination);
	}


	@Override
	protected int getDistanceBetween(Integer node, Integer target) {
		if(complete){
			tempV = graphDb.findNode(Neo4JBenchmark.labelPerson, "name", target);
			for(Relationship rel : graphDb.findNode(Neo4JBenchmark.labelPerson, "name", node).getRelationships(Direction.OUTGOING)){
	
				if(rel.getEndNode().equals(tempV)){
					return (int) rel.getProperty("weight");
				}
			}
			throw new RuntimeException("Impossible");
		}else{
			return 1;
		}
		
	}
	

	@Override
	protected List<Integer> getNeighborsFrom(Integer node) {

		//Prend les voisins qui vont vers node et qui ne sont pas dans P
		neighbors = new ArrayList<Integer>();

		for(Relationship outEdge : graphDb.findNode(Neo4JBenchmark.labelPerson, "name", node).getRelationships(Direction.OUTGOING)){

			if (!seenNodes.contains(outEdge.getEndNode())) {

				neighbors.add((int)outEdge.getEndNode().getProperty("name"));

			}
		}

		return neighbors;
	}
	@Override
	public void getPath() {
		ArrayList<Integer> path = new ArrayList<Integer>();
		Integer step = destinationNode;
	
		if (predecessors.get(step) == null) {
			System.out.println("rien");

		}
		path.add((int) graphDb.findNode(Neo4JBenchmark.labelPerson, "name", step).getProperty("name"));
		while (predecessors.get(step) != null) {
			step = predecessors.get(step);
			path.add((int) graphDb.findNode(Neo4JBenchmark.labelPerson, "name", step).getProperty("name"));
		}
	
		Collections.reverse(path);
		System.out.println(path.toString());
	}

}
