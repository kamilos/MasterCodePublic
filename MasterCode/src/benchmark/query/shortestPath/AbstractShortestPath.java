package benchmark.query.shortestPath;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;



public abstract class AbstractShortestPath<N,E,G> {
	public int iter=0;
	protected G graphDb;
	protected HashSet<N> seenNodes;
	protected HashSet<N> notSeenNodes;
	protected HashMap<N, Integer> distanceFromStartNode;
	protected HashMap<N, N> predecessors;
	protected N startNode;
	protected N destinationNode;
	int distance = 0;
	protected boolean complete;
	protected boolean all;
	private List<N> neighbors ;
	private Iterator<N> it;
	N tempNode;
	N closestNode;
	
	protected long time2;
	protected double time;
	protected long time1;
	
	protected long time11;
	protected long time22;
	protected double timee;
	
	protected long cpt;

	
	protected abstract List<N> getNeighborsFrom(N node);
	protected abstract int getDistanceBetween(N node, N target);
	protected abstract void getPath() ;

	
	public AbstractShortestPath(G graphDb, N start, N destination) {

		this.graphDb = graphDb;
		this.startNode = start;
		this.destinationNode = destination;
		seenNodes = new HashSet<N>(); // Vaut P y ajout les noeuds progessivement
		notSeenNodes = new HashSet<N>();
		distanceFromStartNode = new HashMap<N, Integer>();		
		predecessors = new HashMap<N, N>();
		
		distanceFromStartNode.put(startNode, 0);
		notSeenNodes.add(startNode);
	}

	public void start(){

		N node = startNode;
		while (!notSeenNodes.isEmpty() && iter < 40) { // Tant que noeuds hors de P
//			time1=System.nanoTime();

			node = getClosestNodeFromNotSeenNodes();

//			time2 = System.nanoTime();
//			getClosestTime+=(time2-time1)/Math.pow(10, 9);
			seenNodes.add(node); 
			notSeenNodes.remove(node); // metre le noeud trouvé dans P et le supprimer de P
//			time1=System.nanoTime();

			findMinimalDistances(node);
//			time2 = System.nanoTime();
//			findMini+=(time2-time1)/Math.pow(10, 9);

			iter++;
			//System.out.println(iter);
		}
//		System.out.println("getClosest"+getClosestTime);
//		System.out.println("findMini"+findMini);

		//System.out.println("temps voisins"+time);


	}
	

	protected void findMinimalDistances(N node) {
			//Pour chaque voisin de node qui n'est pas dans seenNoced
			neighbors = getNeighborsFrom(node);
			for (N neighbor : neighbors) {
				//cherche le min entre dist(neighbor) et dist(node) + poids(node, neighbor)
				//on fait une maj entre distance entre start node et neighbor
				distance = getDistanceBetween(node, neighbor);
				if ( getDistanceFromStartNode(node) + distance <= getDistanceFromStartNode(neighbor)) {
					//un plus court chemin a été trouvé
					distanceFromStartNode.put(neighbor, getDistanceFromStartNode(node) + distance);
					predecessors.put(neighbor, node);
					notSeenNodes.add(neighbor);
				}
			}

		
	}
		

	protected int getDistanceFromStartNode(N destination) {
		Integer d = distanceFromStartNode.get(destination);
		return d != null ? d:Integer.MAX_VALUE  ; //Si pas dedans alors infini
		
	}
	
	private N getClosestNodeFromNotSeenNodes() {
		//Choisi un sommet hors de P qui soit le plus proche de start Node
		it = notSeenNodes.iterator();
		closestNode = it.next();
		while(it.hasNext()){
			tempNode = it.next();
			if (getDistanceFromStartNode(tempNode) < getDistanceFromStartNode(closestNode)) {
				closestNode = tempNode;	
			}
		}

		return closestNode;
	}
		
	public int getDistance(){
		
		distance = -1;
		N current = destinationNode;
		if(predecessors.get(current)!= null){
			distance =0;
			while (predecessors.get(current) != null) {
				current = predecessors.get(current);
				distance++;
			}
		}
		return distance;	
	}
	
	public void modeComplete(boolean complete) {
		this.complete=complete;
	}

}
