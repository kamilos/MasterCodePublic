package benchmark.query;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery;
import com.tinkerpop.blueprints.Compare;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientVertex;
import com.tinkerpop.gremlin.Tokens.T;
import com.tinkerpop.gremlin.java.GremlinPipeline;
import com.tinkerpop.pipes.util.structures.Row;

import benchmark.query.shortestPath.OrientDBshortestPath;
import constant.Const;
import lib.Query;

//Note: every time the graph is modified an implicit transaction is started automatically 
// if no previous transaction was running. Transactions are committed automatically when the graph is closed by calling 
// the shutdown() method or by explicit commit()

public class OrientDBQuery extends Query {
	

	//protected OrientGraph graphDb;
	protected OrientGraph graphDb;
	protected long time1;
	protected long time2;


	private int choice;

	private long time11;



	public OrientDBQuery(OrientGraph graphDb2) {
		this.graphDb = graphDb2;
		if(Const.DB_CHOICE.equals(Const.DB_CHOICES.ORIENTDB)){
			System.out.println("NATIF");
		 	choice=0;
		}else{
			System.out.println("OSQL");
			choice=1;
		}

	}

	public void start(ArrayList<Integer> testValues) {
		
		super.start(testValues);
		
		
//		 System.out.println("Nombre de noeuds :" +Iterables.size( graphDb.getVertices())); // pas d'impact sur la recup du noeud selon prop
//		System.out.println("Nombre d'arcs :" +Iterables.size( graphDb.getEdges()));

//		printGraph();
		

	}

	protected void queryGraphMatchingConditionnal() {
		
		List<Row> a = new ArrayList();

		int cpt=0;
		time1 = System.nanoTime();
		for(int i = 0;i<testValues.size();i++){
			a = new GremlinPipeline<Object, Object>(graphDb.getVertices("Person.name",testValues.get(i))).as("a")
			.outE("likes").has("weight", T.lt,400).inV().except("a").has("age",T.gt,18).as("b")
			.out("hates").except("a").except("b").has("age",T.gt,18).as("c").inE("likes").has("weight", T.lt,400).outV().retain("a")
			.select().toList(); //.select().toList();

			cpt += a.size();
			a.clear();
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
		
//		for(int i = 0;i<t.getRowCount();i++){ 
//
//		
//			System.out.print(  "A:" + (Integer)((Vertex)t.get(i).get(0)).getProperty("name"));
//			System.out.print(" B:" + (Integer)((Vertex)t.get(i).get(1)).getProperty("name"));
//			System.out.println(" C:" + (Integer)((Vertex)t.get(i).get(2)).getProperty("name"));
//		}		
	}

	protected void queryGetEdgeByPropertyGroupBy() {

		int cpt=0;
		Iterator b ;
		Iterator<OrientVertex> iterator ;
		time1 = System.nanoTime();

		for(int i = 0;i<testValues.size();i++){
			if(choice == 0){
				b = new GremlinPipeline(graphDb.getEdges()).has("weight", T.gte, testValues.get(i)).has("weight", T.lte, testValues.get(i)+50).property("weight").groupCount().cap().iterator();
				while(b.hasNext()){
					b.next();
					cpt++;
				}
				                 	                                  	
			}else if(choice == 1){
				 iterator = ((Iterable<OrientVertex>) graphDb.command(new OSQLSynchQuery<Object>("select count(*) as size,weight from E where weight >= ? and weight <= ? group by weight")).execute(testValues.get(i),testValues.get(i)+50)).iterator();
				while(iterator.hasNext()){
					iterator.next();
					cpt++;
					//System.out.println((Long)tempVertex.getProperty("size") +" : "+(Integer)tempVertex.getProperty("weight"));
					//System.out.println("--");
				}
			}
		}
		
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+((time2-time1)/Math.pow(10, 9)));
		System.out.println(cpt);				
	}

	
	protected void queryGraphMatching() {
		
		int cpt=0;
		time1 = System.nanoTime(); 
		List<Row> a = new ArrayList<Row>();
		for(int i = 0;i<testValues.size();i++){

			a = new GremlinPipeline<Object, Object>(graphDb.getVertices("Person.name",testValues.get(i))).as("a")
					.out().except("a").as("b")
					.out().except("a").except("b").as("c").in().retain("a").select().range(0, 19).toList(); //.select().toList();
			cpt+=a.size();
			a.clear();
			
//			for(int i = 0;i<t.getRowCount();i++){
//			
//				System.out.print(  "A:" + (Integer)((Vertex)t.get(i).get(0)).getProperty("name"));
//				System.out.print(" B:" + (Integer)((Vertex)t.get(i).get(1)).getProperty("name"));
//				System.out.println(" C:" + (Integer)((Vertex)t.get(i).get(2)).getProperty("name"));
//			}
		}
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	protected void queryKHop() {
		

		
		ArrayList<Vertex> nodesDist1 = new ArrayList<Vertex>();
		ArrayList<Vertex> nodesDist2 = new ArrayList<Vertex>();

		time1 = System.nanoTime();
		int cpt=0;
		List<Object> list ;
		Iterator<OrientVertex> iterator;
		for(int i = 1;i<testValues.size();i++){
			if(choice == 0){
				
				 list = new GremlinPipeline<Object, Object>(
                        graphDb.getVertices("Person.name", testValues.get(i))).as("source")
                               .out("likes").except("source").aggregate(nodesDist1)
                               .out("likes").dedup().except("source").except(nodesDist1).aggregate(nodesDist2)
                               .out("likes").dedup().except("source").except(nodesDist1).except(nodesDist2).property("name")
                               .range(0,49).toList(); //range(0, 9)
			
				cpt+=list.size();
				list.clear();
				nodesDist1.clear();
				nodesDist2.clear();

			}else if (choice==1){
				iterator = ((Iterable<OrientVertex>) graphDb.command(new OSQLSynchQuery<Object>("select * from ( TRAVERSE out('likes') FROM (select * from Person where name = ?) while $depth <= 3 STRATEGY BREADTH_FIRST ) where $depth >= 3 limit 50 ")).execute(testValues.get(i))).iterator();
				
				while(iterator.hasNext()){
					iterator.next();
					cpt++;
				}				
			}
		}

		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println("cpt"+cpt);

		
	}

	protected void queryGetNeighborhoodFilter() {
		Iterator<Vertex> it = null; ;
				
		time1 = System.nanoTime();
		int cpt=0;
		for(int i = 1;i<testValues.size();i++){
			
			if(choice == 0){
				it = new GremlinPipeline<Object, Object>(graphDb.getVertices("Person.name", testValues.get(i))).outE("likes").has("weight", Compare.GREATER_THAN_EQUAL, 200).inV().iterator();
			
			}else if(choice == 1){
				 it=((Iterable<Vertex>)graphDb.command(new OSQLSynchQuery<Vertex>("select expand(outE('likes')[weight>=200].inV()) from Person where name=? ")).execute(testValues.get(i))).iterator();
			}
			if(it!= null){
			 while(it.hasNext()){
				 	it.next();
			 		cpt++;
			    }
			}
			
		}	
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println("cpt: "+cpt);		
			
	}

	protected void queryGetNodeByPropertyRangeGroupBy() {
	
		Iterator<Vertex> it = null; ;
		
		int cpt=0;
		List a;
		Iterator<OrientVertex> iterator = null ;
		time1 = System.nanoTime();
		for(int i = 1;i<testValues.size();i++){
			if(choice == 0){	             
				a = new GremlinPipeline(graphDb.getVertices()).has("age", T.gte, testValues.get(i)).has("age", T.lte, testValues.get(i)+10).property("age").groupCount().cap().toList();

				 a.clear();
				 cpt+=a.size();			                                  	
			}else if(choice == 1){

				iterator = ((Iterable<OrientVertex>) graphDb.command(new OSQLSynchQuery<Object>("select count(*) as size,age from Person where age >= ? and age <= ? group by age")).execute(testValues.get(i),testValues.get(i)+10)).iterator();

				while(iterator.hasNext()){
					iterator.next();
					cpt++;
				}
			}
		}
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9)); 
		System.out.println(cpt);

	}

	protected void queryGetEdgeByProperty() {
		
		Iterator<Edge> it = null; ;
		

		
		time1 = System.nanoTime();
		int cpt=0;
		for(int i = 1;i<testValues.size();i++){
			if(choice == 0){
				it = new GremlinPipeline(graphDb.getEdges()).has("weight", testValues.get(i)).range(0, 99).iterator();		

			}else if(choice == 1){
				it = ((Iterable<Edge>) graphDb.command(new OSQLSynchQuery<Object>("select * from E where weight = ? limit 100")).execute(testValues.get(i))).iterator();
			}
			if(it!=null){
				while(it.hasNext()){
					cpt++;
					it.next();
				}
			}
		}
		
		
		

		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));

		System.out.println(cpt);

		
	}

	protected void printGraph() {
		for (Vertex n : graphDb.getVertices()) {
			System.out.print(n + ":");
			for (String key : n.getPropertyKeys())
				System.out.print(key + "=" + n.getProperty(key) + ", ");
			System.out.println();
		}
		
		System.out.println();

		for (Edge r : graphDb.getEdges()){
			Vertex n1 = r.getVertex(Direction.OUT);
			Vertex n2 = r.getVertex(Direction.IN);
			System.out.print(n1.getProperty("name") + " --> " + n2.getProperty("name") + " (" + r.getLabel() + ") ");
			for (String key : r.getPropertyKeys())
				System.out.print(key + "=" + r.getProperty(key) + ", ");
				System.out.print(" id = " + r.getId());


			System.out.println();

		}		
	}

	protected void queryGetNodeByPropertyRange() {

		Iterator<Vertex> it = null; ;
		

				
		time1 = System.nanoTime();
		int cpt=0;
		for(int i = 1;i<testValues.size();i++){
			
			if(choice == 0){		
				it = new GremlinPipeline(graphDb.getVertices()).has("age", T.gte, testValues.get(i)).has("age", T.lte, testValues.get(i)+10).range(0, 499);//.range(0, 20);

			}else if(choice == 1){
				it = ((Iterable<Vertex>) graphDb.command(new OSQLSynchQuery("select * from Person where age >= ? and age <= ? limit 500")).execute(testValues.get(i),testValues.get(i)+10)).iterator();
			}
			
			while(it.hasNext()){
				it.next();
				cpt++;
			}
		}

		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);

		
			
	}

	protected void queryGetEdgeByPropertyRange() {
		

		Iterator<Edge> it = null; ;

		
		time1 = System.nanoTime();
		
		
		int cpt=0; OrientVertex res;
		for(int i = 1;i<testValues.size();i++){
			if(choice == 0){
				it = new GremlinPipeline(graphDb.getEdges()).has("weight", T.gte, testValues.get(i)).has("weight", T.lte, testValues.get(i)+50).range(0, 999).iterator();
			}else if (choice == 1){
				it = ((Iterable<Edge>) graphDb.command(new OSQLSynchQuery<Object>("select * from E where weight >= ? and weight <= ? limit 1000 ")).execute(testValues.get(i),testValues.get(i)+50)).iterator();
	
			}
			
			while(it.hasNext()){
				cpt++;
				it.next();

			}

		}
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);

	}

	protected void queryPageRank() {
		
		
		double dampingfactor = 0.85;
		double leftPart = (1.0 - dampingfactor);


		int iter = 6;
		double prSumValue;
		OrientVertex tempV;
		Iterator<Edge> tempE;
		int links;
		
		time11 = System.nanoTime();
		long nv = graphDb.countVertices();	
	      
		double initValue = 1.0/nv;
		for (Vertex v : graphDb.getVertices()) {
			v.setProperty("pageRank", initValue);
			tempE = v.getEdges(Direction.OUT).iterator();

			links = 0;
			while(tempE.hasNext()){
				tempE.next();
				links++;
			};
			v.setProperty("outNodes", links);
		}

		while (iter > 0) {
			for (Vertex v : graphDb.getVertices()) {
				prSumValue = 0;
				for (Edge edge : ((OrientVertex) v).getEdges(Direction.IN)) { 
					tempV = (OrientVertex) edge.getVertex(Direction.OUT);
					prSumValue += (Double)tempV.getProperty("pageRank") / (int)tempV.getProperty("outNodes");
				}
	    		v.setProperty("pageRank", leftPart + (dampingfactor * prSumValue));
			}	
			iter--;
		}

		time2=System.nanoTime();
		System.out.println("Temps écoule : "+(time2-time11)/Math.pow(10, 9));

	}

	protected void queryGetNeighborhood() {
		
		Iterator<Vertex> it = null; ;
		
		
		time1 = System.nanoTime();
		int cpt=0;
		for(int i = 1;i<testValues.size();i++){
			
			if(choice == 0){
				it = new GremlinPipeline<Object, Object>(graphDb.getVertices("Person.name", testValues.get(i))).out().iterator();
			
			}else if(choice == 1){
				 it=((Iterable<Vertex>)graphDb.command(new OSQLSynchQuery<Vertex>("select expand(out()) from Person where name=? ")).execute(testValues.get(i))).iterator();
			}
			if(it!= null){
			 while(it.hasNext()){
				 	it.next();
			 		cpt++;
			    }
			}
			
		}	

		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println("cpt: "+cpt);	

	}

	protected void queryShortestPath() {
		Iterator<Vertex> vertex1it;
		Iterator<Vertex> vertex2it;
		int cpt =0;
		int found = 0;
		time1 = System.nanoTime();
		int dist ;

		OrientDBshortestPath shortest;
		for(int i = 0;i<testValues.size()-1;i++){
			System.out.println("next");
			vertex1it = graphDb.getVertices("Person.name", testValues.get(i)).iterator();
			vertex2it = graphDb.getVertices("Person.name", testValues.get(i+1)).iterator();

			if(vertex1it.hasNext() && vertex2it.hasNext() ){
				 shortest = new OrientDBshortestPath(graphDb,vertex1it.next().getProperty("name") ,vertex2it.next().getProperty("name"));
				shortest.modeComplete(false);
				shortest.start();
				

				 dist = shortest.getDistance();
				if(dist!=-1){
					cpt+=dist;

					found++;
				}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println("nombres de noeuds"+cpt);	
		System.out.println("found"+found);	
	    	
	}

	protected void queryDeleteNode() {
				
		ArrayList<Vertex> toDelete1 =new ArrayList<Vertex>();
		for(int i = 1;i<=testValues.size();i++){
			toDelete1.add(graphDb.getVertices("Person.name", -i).iterator().next());	
		}
		
		Iterator<Vertex> toDeleteIt = toDelete1.iterator();
		int i =0;
		time1=System.nanoTime();
		if(choice==0){
			while(toDeleteIt.hasNext()){
				toDeleteIt.next().remove(); //Supprime automatiquement ses relation liées	
				i++;
				//if(i%testValues.size()0==0)
					graphDb.commit();
			}				
		}else{
			while(toDeleteIt.hasNext()){
				graphDb.command(new OCommandSQL("DELETE VERTEX ?")).execute(toDeleteIt.next().getId());
				i++;
				//if(i%testValues.size()0==0)
					graphDb.commit();
			}
		}
			
		graphDb.commit();
		time2=System.nanoTime();
		System.out.print("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		
		

	}



	protected void queryDeleteEdge() {
		
				

		ArrayList<Edge> toDelete1 =new ArrayList<Edge>();
		ArrayList<Object> toDelete2 =new ArrayList<Object>();
		for(int i = 1;i<testValues.size();i++){
				
			if(choice ==0){
				toDelete1.add(graphDb.getVertices("Person.name", -i).iterator().next().getEdges(Direction.OUT).iterator().next());
			}else{
				toDelete2.add(graphDb.getVertices("Person.name", -i).iterator().next());
				toDelete2.add(graphDb.getVertices("Person.name", -i-1).iterator().next());
			}
			
		}
		Iterator<Edge> toDeleteIt = toDelete1.iterator();
		Iterator<Object> toDeleteIt2 = toDelete2.iterator();
		
		time1=System.nanoTime();
		int i=0;
		if(choice==0){
			while(toDeleteIt.hasNext()){
				graphDb.removeEdge(toDeleteIt.next());
				i++;
				//if(i%testValues.size()0==0)
					graphDb.commit();
			}
		}else{
			while(toDeleteIt2.hasNext()){
				graphDb.command(new OCommandSQL("DELETE EDGE FROM ? TO ?")).execute(toDeleteIt2.next(),toDeleteIt2.next());
				i++;
				//if(i%testValues.size()0==0)
					graphDb.commit();
			}
			
		
		}
		graphDb.commit();
		time2=System.nanoTime();

		double average = ((time2-time1)/Math.pow(10, 9));
		System.out.print(average);

		

	}

	protected void queryUpdateProperty() {
				

		ArrayList<Vertex> toDelete1 =new ArrayList<Vertex>();
		
		for(int i = 1;i<=testValues.size();i++){
				toDelete1.add(graphDb.getVertices("Person.name", -i).iterator().next());
		}
		time1=System.nanoTime();
		for(int i = 0;i<testValues.size();i++){
			if(choice == 0){
				toDelete1.get(i).setProperty("age", 18);
			}else{
				graphDb.command(new OCommandSQL("UPDATE ? SET age=18")).execute(toDelete1.get(i).getId());			
			}
			//if(i%10==0)
				graphDb.commit();
		}
		graphDb.commit();
		time2=System.nanoTime();
		
		System.out.print("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		

	}



	protected void queryAddEdge() {
						
		ArrayList<OrientVertex> toAdd =new ArrayList<OrientVertex>();
		for(int i = 1;i<testValues.size();i++){
				
			toAdd.add((OrientVertex) graphDb.getVertices("Person.name", -i).iterator().next());
			toAdd.add((OrientVertex) graphDb.getVertices("Person.name", -i-1).iterator().next());
				
		}				

		Iterator<OrientVertex> toAddIt = toAdd.iterator();
		
		time1=System.nanoTime();
		int i=0;
		while(toAddIt.hasNext()){
			if(choice == 0){
				graphDb.addEdge(null, toAddIt.next(), toAddIt.next(), "likes").setProperty("weight", -1);

			}else{
				graphDb.command(new OCommandSQL("CREATE EDGE likes FROM ? TO ? SET weight = -1")).execute(toAddIt.next().getId(),toAddIt.next().getId());
			}
			i++;
			//if(i%10==0)
			graphDb.commit();
		}
			
		time2=System.nanoTime();

		System.out.print("Temps écoule : "+((time2-time1)/Math.pow(10, 9)));

		
	}

	protected void queryAddNode() {
		
		time1 = System.nanoTime();
		
		for(int i = 1;i<=testValues.size();i++){
			if(choice==0){
				graphDb.addVertex("class:Person").setProperties("name",-i,"age",-1);
			}else{
				graphDb.command(new OCommandSQL("CREATE VERTEX Person SET name = ?, age = -1")).execute(-i);
			}
			//if(i%testValues.size()==0)	{
			graphDb.commit();
			//}
		}
		
		time2=System.nanoTime();
		System.out.print("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		
		
		
	}

	

	

	protected void queryGetNodeByProperty() {
				
		Iterator<Vertex> it = null ;
		
		int cpt=0;
		
		time1 = System.nanoTime();
		
		for(int i = 1;i<testValues.size();i++){
			 if(choice == 0){		
				it= graphDb.getVertices("Person.name",testValues.get(i)).iterator();
			}else if(choice == 1){
				it = ((Iterable<Vertex>) graphDb.command(new OSQLSynchQuery("select * from Person where name=?")).execute(testValues.get(i))).iterator();
			}
			while(it.hasNext()){
				it.next();
				cpt++;
			} 			
		}

		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println("cpt"+cpt);

		
	}
	
	@Override
	protected void queryUpdatePropertyWeight() {
		ArrayList<Edge> toDelete1 =new ArrayList<Edge>();
		ArrayList<Object> toDelete2 =new ArrayList<Object>();
		for(int i = 1;i<testValues.size();i++){
				
			if(choice ==0){
				toDelete1.add(graphDb.getVertices("Person.name", -i).iterator().next().getEdges(Direction.OUT).iterator().next());
			}else{
				toDelete2.add(graphDb.getVertices("Person.name", -i).iterator().next());
				toDelete2.add(graphDb.getVertices("Person.name", -i-1).iterator().next());
			}
			
		}
		Iterator<Edge> toDeleteIt = toDelete1.iterator();
		Iterator<Object> toDeleteIt2 = toDelete2.iterator();
		
		time1=System.nanoTime();
		int i=0;
		if(choice==0){
			while(toDeleteIt.hasNext()){
				toDeleteIt.next().setProperty("weight", -1);
				i++;
				//if(i%testValues.size()0==0)
					graphDb.commit();
			}
		}else{
			while(toDeleteIt2.hasNext()){
				graphDb.command(new OCommandSQL("UPDATE ? SET weight=-1")).execute(toDeleteIt2.next(),toDeleteIt2.next());
				i++;
				//if(i%testValues.size()0==0)
					graphDb.commit();
			}
			
		
		}
		graphDb.commit();
		time2=System.nanoTime();

		double average = ((time2-time1)/Math.pow(10, 9));
		System.out.print(average);		
	}

	@Override
	protected void queryShortestPathWeight() {
		Iterator<Vertex> vertex1it;
		Iterator<Vertex> vertex2it;
		int cpt =0;
		int found = 0;
		time1 = System.nanoTime();
		int dist ;

		OrientDBshortestPath shortest;
		for(int i = 0;i<testValues.size()-1;i++){
			System.out.println("next");
			vertex1it = graphDb.getVertices("Person.name", testValues.get(i)).iterator();
			vertex2it = graphDb.getVertices("Person.name", testValues.get(i+1)).iterator();

			if(vertex1it.hasNext() && vertex2it.hasNext() ){
				 shortest = new OrientDBshortestPath(graphDb,vertex1it.next().getProperty("name") ,vertex2it.next().getProperty("name"));
				shortest.modeComplete(true);
				shortest.start();
				

				 dist = shortest.getDistance();
				if(dist!=-1){
					cpt+=dist;

					found++;
				}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println("nombres de noeuds"+cpt);	
		System.out.println("found"+found);	


	}

	

	

	

}
