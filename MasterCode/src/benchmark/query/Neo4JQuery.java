package benchmark.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Uniqueness;

import benchmark.Neo4JBenchmark;
import benchmark.query.shortestPath.Neo4jShortestPath;
import lib.Query;


public class Neo4JQuery extends Query{

	protected GraphDatabaseService graphDb;
	protected Label labelPerson = DynamicLabel.label("Person");
	protected long time1;
	protected long time2;
	private double compteVoisin;
	private double majProp;
	private long time11;
	private long time111;
	
	public enum RelTypes implements RelationshipType {
		Likes, Hates
	}

	public Neo4JQuery(GraphDatabaseService graphDb) {
		this.graphDb = graphDb;
		//printGraph();
	}

	public void start(ArrayList<Integer> testValues) {
		
		super.start(testValues);
		
//		try (Transaction tx = graphDb.beginTx()) {
//			System.out.println("Nombre de noeuds : "+ IterablesUtil.size(graphDb.getAllNodes()));
//			System.out.println("Nombre d'arcs  : "+ IterablesUtil.size(graphDb.getAllRelationships()));
//		}
		

//		time1 = System.nanoTime();
//		int cpt = 0;
//		// Le mettre ici ou pour chaque truc n'influence pas les perf
//				 
//			AbstractShortestPathNeo4J neo =  new AbstractShortestPathNeo4J(graphDb);
//			cpt+=neo.start(testValues);	
//		//}
//		time2=System.nanoTime();
//		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
//		System.out.println("Neo4J : Fin des Tests");
//		System.out.println(cpt);

	}



	protected void queryGetNodeByPropertyRange() {
		int prop;
		int cpt=0;
		time1 = System.nanoTime();
		int limit;
		//ArrayList<Integer> list = new ArrayList<Integer>();
		
		try (Transaction tx = graphDb.beginTx()) {
			for(int currentfilter = 1;currentfilter<testValues.size();currentfilter++){
				limit = 0;
				for(Node n : graphDb.getAllNodes()) {
					prop = (Integer) n.getProperty(Neo4JBenchmark.propertyAge);
					if( prop>= testValues.get(currentfilter) && prop<=(testValues.get(currentfilter)+10) ){
						n.getProperty("name");
						cpt++;
						limit++;
						if(limit>500){	
							cpt--;
							break;
						}
					}
				}
			}
		}	
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	protected void queryGetNeighborhoodFilter() {
		int cpt = 0;
		Node startNode;

		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {

			for(int currentnode = 1;currentnode<testValues.size();currentnode++){
				startNode = graphDb.findNode(labelPerson, "name", testValues.get(currentnode));

				if(startNode!=null){
					for(Relationship rel :  startNode.getRelationships(Direction.OUTGOING, RelTypes.Likes)){
						if((int)rel.getProperty("weight")>=200){
							rel.getEndNode();
							cpt++;
						}
					}
				}

				
				
			}
		}
			time2=System.nanoTime();
			System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
			System.out.println(cpt);
			
	}



	protected void queryKHop() {
		Iterator<Node> it = null; 
		Node startNode;
		Iterator<Relationship> it2;

		int cpt=0;
		TraversalDescription td;
		time1 = System.nanoTime();
		int limit;
		try (Transaction tx = graphDb.beginTx()) {
			for(int currentfilter = 1;currentfilter<testValues.size();currentfilter++){
					limit =0;
					startNode = graphDb.findNode(labelPerson, "name",testValues.get(currentfilter) );
					if(startNode!=null){
						 td = graphDb.traversalDescription()
								.breadthFirst()
								.relationships( Neo4JBenchmark.RelTypes.Likes, Direction.OUTGOING )
								.uniqueness( Uniqueness.NODE_GLOBAL )
								.evaluator(Evaluators.excludeStartPosition()).evaluator(Evaluators.fromDepth(3)).evaluator(Evaluators.toDepth(3));
						ResourceIterator<Node> traverser = td.traverse(startNode).nodes().iterator();
					
						while(traverser.hasNext()){
								//System.out.println(((Node)traverser.next()).getProperty("name"));
								traverser.next().getProperty("name");
								cpt++;
								limit++;
								if(limit>50){
									break;
								}
								
						}
						}
										
				}		
				
		}
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	
	}

	protected void queryGetEdgeByProperty() {
		int cpt=0;
		time1 = System.nanoTime();
		int limit = 0;
		try (Transaction tx = graphDb.beginTx()) {
			for(int currentnode = 1;currentnode<testValues.size();currentnode++){
				limit=0;
				for(Relationship r : graphDb.getAllRelationships()) {
					
					if( (int) r.getProperty(Neo4JBenchmark.propertyWeight) == testValues.get(currentnode)){
						cpt++;
						limit++;
						if(limit>100){
							cpt--;
							break;
						}
					}
					
				}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
						
	}

	protected void queryGetEdgeById() {
		int cpt=0;
		time1 = System.nanoTime();
		Relationship res;
		try (Transaction tx = graphDb.beginTx()) {
			res = graphDb.getRelationshipById(1);
			//System.out.println(res.getProperty("weight"));
		}	
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	protected void printGraph() {
		try (Transaction tx = graphDb.beginTx()) {


		for (Node n : graphDb.getAllNodes()) {
			System.out.print(n + ":");
			for (String key : n.getPropertyKeys())
				System.out.print(key + "=" + n.getProperty(key) + ", ");
			System.out.println();
		}

		for (Relationship r : graphDb.getAllRelationships()) {
			Node n1 = r.getStartNode();
			Node n2 = r.getEndNode();
			System.out.print(n1.getProperty("name") + " --> " + n2.getProperty("name") + " (" + r.getType() + ") ");
			for (String key : r.getPropertyKeys())
				System.out.print(key + "=" + r.getProperty(key) + ", ");
				System.out.print(" id = " + r.getId());


			System.out.println();
		}
		}

	}

	protected void queryClusteringCoefficient() {
		double LCCSum = 0.0;
		double LCCValue =0.0; //peut contenir plus que float (32 bits)
	    Set<Node> neighbours;
	    long Nv = 0; //nombre de liens entre les voisins de V
	    
	    int numNodes = 0;
	    HashMap<Long, Double> LCC = new HashMap<Long, Double>();
	    int cpt=0;
		time1 = System.nanoTime();
		try (Transaction transaction = graphDb.beginTx()) {
			
			for (Node v : graphDb.getAllNodes()) {//pour chaque noeud
				
				//set pour savoir si object contenu dedans, set = no access position, mais contains o(1) vs o(n), et no duplicates
				neighbours = new HashSet<>(); 
				
				//Récupères les voisins du noeud courant
				for (Relationship edge : v.getRelationships()) {
					neighbours.add(edge.getOtherNode(v));
				}
				
				if (neighbours.size() <= 1){ // 1 car on on compte v. Si pas d'autre voisins, ca vaut 0, et c'est un star
					LCCValue = 0.0;
				}else{
				
					Nv = 0;
					//Pour chaque voisin, regarde s'il est connecté à un autre voisin, si oui inc le Nv
					for (Node neighbour : neighbours) {
						//Recupères les noeuds connectés au voisin courant
						
						//ici direction importante car éviter de compter le lien 2->3 deux fois
						for (Relationship edge : neighbour.getRelationships(Direction.OUTGOING)) { 
							
							// Regarde si les noeuds voisins du voisin font partie des voisin de V, si oui alors il y a connection
							if (neighbours.contains(edge.getOtherNode(neighbour))) {
								Nv++;
							}
						}
					}
					LCCValue = (2.0 * Nv) / (neighbours.size() * (neighbours.size() - 1));
				}
				LCC.put(v.getId(), LCCValue);

				LCCSum+=LCCValue;
				numNodes++;
			}
		
			System.out.println(LCCSum/numNodes);
	
			System.out.println(LCC.get(graphDb.findNode(labelPerson, "name", 1).getId())); //0.16666666666666666
			System.out.println(LCC.get(graphDb.findNode(labelPerson, "name", 2).getId())); //0.3333333333333333

		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	protected void queryPageRank() {

	
		//compte pas lien vers soi même
      double dampingfactor = 0.85;
      //HashMap<Long, Double> pr = new HashMap<Long, Double>();
      HashMap<Long, Integer> degree = new HashMap<Long, Integer>();

      // the sum of all web pages' PageRanks will be one.
      double prSumValue = 0;
      int outgoingLinks;
      double leftPart = ( (1.0-dampingfactor)); //Version originale //dans autres version divise par nv
      int iter = 6;//testValues.size(); // According to publications of Lawrence Page and Sergey Brin, about testValues.size() iterations 
      Node endNode;
      
      	time11 = System.nanoTime();

      	try (Transaction tx = graphDb.beginTx()) {
    	   
    	  int nv=0;
    	  ResourceIterator<Node> nvIt = graphDb.getAllNodes().iterator();
    	  while(nvIt.hasNext()){
    		  nvIt.next();
    		  nv++;
    	  }
	      time2=System.nanoTime();
	      System.out.println("Compatage Temps écoule : "+(time2-time11)/Math.pow(10, 9));
	      time1 = System.nanoTime();

    	  double initValue = 1/nv;
	      for(Node v : graphDb.getAllNodes()) {
	    	  //pr.put(v.getId(), initValue);
	    	  v.setProperty("pageRank", initValue );
			  outgoingLinks = v.getDegree(Direction.OUTGOING); 

			degree.put(v.getId(), outgoingLinks);

	      }
	      time2=System.nanoTime();
	      System.out.println("init pr Temps écoule : "+(time2-time1)/Math.pow(10, 9));
	      time1 = System.nanoTime();
      		
	      while(iter > 0) {

			      time111 = System.nanoTime();
	
		    	  for(Node v : graphDb.getAllNodes()) {
		    		  	prSumValue = 0;
	
		    		  for(Relationship rel : v.getRelationships(Direction.INCOMING)) { //compte la relation 1<-1
		    			  endNode = rel.getStartNode(); // prend tous les noeud entrants
		    			  
		    			  // pour chaque noeud entrant, calcule son nombre de noeud sortants
		    			  //prSumValue +=  pr.get(endNode.getId())  / degree.get(endNode.getId());
		    			  prSumValue += (Double)endNode.getProperty("pageRank") / degree.get(endNode.getId());
		    		  }
		    		  time2=System.nanoTime();
				      compteVoisin+=(time2-time1)/Math.pow(10, 9);
				      time1 = System.nanoTime();
		    		  //pr.put(v.getId(),leftPart + (dampingfactor * prSumValue));
		    		  v.setProperty("pageRank", leftPart + (dampingfactor * prSumValue)); // Le probleme est la
		    		  time2=System.nanoTime();
				      majProp+=(time2-time1)/Math.pow(10, 9);
				      time1 = System.nanoTime();
		    	  }
		
		    	  iter--;
		        	
		    	  time2=System.nanoTime();
	    	      System.out.println("fin it prop Temps écoule : "+(time2-time111)/Math.pow(10, 9));
	    	      time1 = System.nanoTime();
	        	}
	      }
	      
	      time2=System.nanoTime();
	      System.out.println("Temps écoule : "+(time2-time11)/Math.pow(10, 9));
			System.out.println("compte voisins" + compteVoisin);
			System.out.println("maj prop" + majProp);
//	      System.out.println(pr.get(graphDb.findNode(labelPerson, "name", 1).getId())); 
//	      System.out.println(pr.get(graphDb.findNode(labelPerson, "name", 2).getId())); 
//	      System.out.println(pr.get(graphDb.findNode(labelPerson, "name", 3).getId())); //1.18
	      //System.out.println(pr.get(graphDb.findNode(labelPerson, "name", 4).getId()));
	      //System.out.println(pr.get(graphDb.findNode(labelPerson, "name", 5).getId()));


      
     
		
	}



	
	protected void queryShortestPath() {
		
		int cpt=0;
		Node node1;
		Node node2;
		Neo4jShortestPath shortest;
		int dist;
		time1 = System.nanoTime();
		int found =0;
		try (Transaction tx = graphDb.beginTx()) {

			for (int i=0;i<testValues.size()-1;i++){
				node1 = graphDb.findNode(labelPerson,"name", testValues.get(i));
				node2 = graphDb.findNode(labelPerson,"name", testValues.get(i+1));
				
				if(node1 != null && node2!= null ){

					shortest = new Neo4jShortestPath(graphDb,(int)node1.getProperty("name"),(int)node2.getProperty("name"));
					shortest.modeComplete(false);
					shortest.start();
					dist=shortest.getDistance();
					if(dist!=-1){
						cpt+=dist;
						found++;
					}
				}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);	
		System.out.println("found" + found);
	
	}

	

	protected void queryUpdateProperty() {
		// Pas de methode update, il suffit de remettre la propriété
		
		ArrayList<Node> toDelete1 =new ArrayList<Node>();
		try (Transaction tx = graphDb.beginTx()) {

			for(int i = 1;i<=testValues.size();i++){ 
				toDelete1.add( graphDb.findNode(labelPerson, "name", -i));
			}
			
			time1=System.nanoTime();
			for(int i = 0;i<testValues.size();i++){
				toDelete1.get(i).setProperty("age", 18);

			}
			tx.success();
		}
		
		time2=System.nanoTime();
		
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
	}
	
	protected void queryUpdatePropertyWeight() {
		// Pas de methode update, il suffit de remettre la propriété
		
		ArrayList<Relationship> toDelete1 =new ArrayList<Relationship>();
		Iterator<Relationship> toDeleteIt;
		try (Transaction tx = graphDb.beginTx()) {

			for(int i = 1;i<testValues.size();i++){ 
				toDelete1.add( graphDb.findNode(labelPerson, "name", -i).getRelationships(Direction.OUTGOING).iterator().next());
			}
			toDeleteIt = toDelete1.iterator();
			
			time1=System.nanoTime();
			
			while(toDeleteIt.hasNext()){
				toDeleteIt.next().setProperty("weight", -1);
			}
			
			tx.success();
		}
		
		time2=System.nanoTime();
		
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
	}




	protected void queryDeleteEdge() {
		Iterator<Relationship> toDeleteIt;
		ArrayList<Relationship> toDelete1 = new ArrayList<Relationship>();
		try (Transaction tx = graphDb.beginTx()) {
	
			for(int i = 1;i<testValues.size();i++){
				toDelete1.add(graphDb.findNode(labelPerson, "name", -i).getRelationships(Direction.OUTGOING).iterator().next());
			}
			toDeleteIt = toDelete1.iterator();
			
			time1=System.nanoTime();
			
			while(toDeleteIt.hasNext()){
				toDeleteIt.next().delete();
			}
			tx.success();
		}
		time2=System.nanoTime();

		double average = (time2-time1)/Math.pow(10, 9);
		System.out.println(average);

	}

	protected void queryDeleteNode() {

		ArrayList<Node> toDelete1 =new ArrayList<Node>();
		Node toDelete;
		Iterator<Relationship> iter;
		try (Transaction tx = graphDb.beginTx()) {

			for(int i = 1;i<=testValues.size();i++){
				toDelete1.add(graphDb.findNode(labelPerson, "name", -i));	
			}
			
			Iterator<Node> toDeleteIt = toDelete1.iterator();
			
			time1=System.nanoTime();
			
			//Note: Besoin de virer les relations avant
			while(toDeleteIt.hasNext()){
				toDelete = toDeleteIt.next();
				iter = toDelete.getRelationships().iterator();
				while (iter.hasNext()) {
					iter.next().delete();
				}
				
				toDelete.delete();
			}		
			tx.success();
		}		
		time2=System.nanoTime();
		double average = ((time2-time1)/Math.pow(10, 9));

		System.out.println(average);

	}

	protected void queryAddEdge() {
		Iterator<Node> toDeleteIt;
		ArrayList<Node> toDelete =new ArrayList<Node>();
		try (Transaction tx = graphDb.beginTx()) {

			for(int i = 1;i<testValues.size();i++){	
				toDelete.add( graphDb.findNode(labelPerson, "name", -i));
				toDelete.add( graphDb.findNode(labelPerson, "name", -i-1));
			}
			toDeleteIt = toDelete.iterator();
			
			time1=System.nanoTime();
			while(toDeleteIt.hasNext()){
				toDeleteIt.next().createRelationshipTo(toDeleteIt.next(), Neo4JBenchmark.RelTypes.Likes).setProperty("weight", -1);
			}	
			tx.success();
		}
		time2=System.nanoTime();

		System.out.println(((time2-time1)/Math.pow(10, 9)));
	}

	protected void queryAddNode() {

		Node res;
		time1 = System.nanoTime();	

		try (Transaction tx = graphDb.beginTx()) {
			for(int i = 1;i<=testValues.size();i++){
				res = graphDb.createNode(labelPerson);
				res.setProperty("name", -i);
				res.setProperty("age", -1);
			}
			tx.success();
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));

		
	}

	protected void queryGetNeighborhood() {

		Node startNode;
		Iterator<Relationship> it2;
		
		int cpt=0;

		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {
			for(int currentfilter = 1;currentfilter<testValues.size();currentfilter++){
					startNode = graphDb.findNode(labelPerson, "name", testValues.get(currentfilter));
					//System.out.println("Pour"+startNode.getProperty("name"));
					if(startNode != null){
						it2 = startNode.getRelationships(Direction.OUTGOING).iterator();
						while (it2.hasNext()) {
							//System.out.println(it2.next().getEndNode().getProperty("name"));
							it2.next().getEndNode();
							cpt++;
						}
					}
			}
		}
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);		

						
	}

	protected void queryGetNodeByProperty() {
		Node res;
		int cpt=0;
		time1 = System.nanoTime();
		
		try (Transaction tx = graphDb.beginTx()) {
			for(int currentnode = 1;currentnode<testValues.size();currentnode++){
				res = graphDb.findNode(labelPerson, "name", testValues.get(currentnode));
				if(res!= null){
					cpt++;
					res.getProperty("name");
					res.getProperty("age");
				}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	protected void queryGetNodeById() {
		Node res;
		int cpt=0;
		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {
			res = graphDb.getNodeById(0);
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}


	

	@Override
	protected void queryGetNodeByPropertyRangeGroupBy() {
		List<Integer> allPeople; //  list of all people
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();

		int prop;
		int cpt=0;
		time1 = System.nanoTime();
		int limit;
		int key;
		int i=0;
		try (Transaction tx = graphDb.beginTx()) {
			for(int currentfilter = 1;currentfilter<testValues.size();currentfilter++){
				limit =0;
				map.clear();
				for(Node n : graphDb.getAllNodes()) {
					prop = (Integer) n.getProperty(Neo4JBenchmark.propertyAge);
					if( prop>= testValues.get(currentfilter) && prop<=testValues.get(currentfilter)+10 ){
						   key = prop;
						   if (!map.containsKey(prop)) {
						      map.put(prop, 0);
						   }else{
							  map.put(prop,map.get(prop)+1);
						   }
						cpt++;

					}
				}
				i++;
			}
		}	
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);		
	}

	@Override
	protected void queryGetEdgeByPropertyRange() {
		int prop;
		int cpt=0;
		time1 = System.nanoTime();
		int limit;
		try (Transaction tx = graphDb.beginTx()) {
			for(int currentfilter = 1;currentfilter<testValues.size();currentfilter++){
				limit=0;
				for(Relationship e : graphDb.getAllRelationships()) {
					prop = (Integer) e.getProperty(Neo4JBenchmark.propertyWeight);
					if( prop>= testValues.get(currentfilter) && prop<=(testValues.get(currentfilter)+50) ){
						cpt++;
						limit++;
						if(limit > 1000){
							cpt--;
							break;
						}
					}
					

				}
			}
		}	
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);		
	}

	@Override
	protected void queryGetEdgeByPropertyGroupBy() {
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();

		int prop;
		int cpt=0;
		int key;
		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {
			for(int currentfilter = 1;currentfilter<testValues.size();currentfilter++){
				map.clear();
				for(Relationship e : graphDb.getAllRelationships()) {
					prop = (Integer) e.getProperty(Neo4JBenchmark.propertyWeight);
					if( prop>= testValues.get(currentfilter) && prop<=(testValues.get(currentfilter)+50) ){
						   key = prop;
						   if (!map.containsKey(prop)) {
						      map.put(prop, 0);
								cpt++;

						   }else{
							  map.put(prop,map.get(prop)+1);
						   }
					}
					

				}
			}
		}	
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);			
	}

	
	@Override
	protected void queryGraphMatchingConditionnal() {
		
	}

	@Override
	protected void queryShortestPathWeight() {

		
		int cpt=0;
		Node node1;
		Node node2;
		Neo4jShortestPath shortest;
		int dist;
		time1 = System.nanoTime();
		int found =0;
		try (Transaction tx = graphDb.beginTx()) {

			for (int i=0;i<testValues.size()-1;i++){
				node1 = graphDb.findNode(labelPerson,"name", testValues.get(i));
				node2 = graphDb.findNode(labelPerson,"name", testValues.get(i+1));
				
				if(node1 != null && node2!= null ){

					shortest = new Neo4jShortestPath(graphDb,(int)node1.getProperty("name") ,(int)node2.getProperty("name"));
					shortest.modeComplete(true);
					shortest.start();
					dist=shortest.getDistance();
					if(dist!=-1){
						cpt+=dist;
						found++;
					}
				}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);	
		System.out.println("found" + found);		

	}




}
