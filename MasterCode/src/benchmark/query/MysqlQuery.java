package benchmark.query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import benchmark.query.shortestPath.SQLShortestPath;
import lib.Query;

public class MysqlQuery extends Query{

	protected long time1;
	protected long time2;
	protected Connection connection;
	protected PreparedStatement preparedStatement;

	public MysqlQuery() {

	}

	public void start(ArrayList<Integer> testValues) {
		super.start(testValues);

//		prepareQuery("select count(*) as size from node"); ResultSet res = executeQuery(); try {res.next(); System.out.println("Nodes : " + res.getInt("size"));} catch (SQLException e) {} 
//		prepareQuery("select count(*) as size from edge"); res = executeQuery(); try {res.next(); System.out.println("Edges : " + res.getInt("size"));} catch (SQLException e) {} 
		 

		System.out.println("MYSQL : Fin des Tests");

	}

	

	protected void queryUpdateProperty() {
		int cpt=0;

		time1=System.nanoTime();
		prepareQuery("update node set age = ? where name = ?");

		for(int i = 1;i<=testValues.size();i++){	
			setInt(1,18); //In Node
			setInt(2,-i); //Out Node
			updateQuery();

		}
		time2=System.nanoTime();
		double average = ((time2-time1)/Math.pow(10, 9));

		System.out.println(average);
	}

	protected void queryGraphMatchingConditionnal() {
		System.out.println("Graph Matching Cond");
		int cpt=0;

		ResultSet res = null ;
		
		time1=System.nanoTime();
		
		
		for(int i = 0;i<testValues.size();i++){	
			prepareQuery("SELECT distinct A1Edge.inNode as A, BEdge.inNode as B, BEdge.outNode as C from node A, node B, node C, edge A1Edge, edge A2Edge, edge BEdge"
					+ " where A.name = ? and A1Edge.inNode = A.name and A1Edge.outNode = BEdge.inNode"
					+ " "
					+ " and A2Edge.inNode = A1Edge.inNode and A2Edge.outNode = BEdge.outNode"
					+ " and A1Edge.outNode <> A1Edge.inNode and BEdge.outNode <> BEdge.inNode and A2Edge.outNode <> A2Edge.inNode "
					+ " and A1Edge.outNode <> A2Edge.outNode and A1Edge.type = 'likes' and A2Edge.type = 'likes'  and BEdge.type = 'hates' "
					+ " and A1Edge.weight< 400 and A2Edge.weight< 400 and C.name = BEdge.outNode and B.name = BEdge.inNode and C.age > 18 and B.age >18 "
					+ " "
					+ " ");
		
			setInt(1,testValues.get(i));		
	
			res = executeQuery();
			
			try {
				while(res.next()){
//					System.out.print("A:" + res.getInt("A"));
//					System.out.print(" B:" + res.getInt("B"));
//					System.out.println(" C:" + res.getInt("C"));
					cpt++;
				}
	
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		try {
			res.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}



	protected void queryGetKHopFilter() {
		
		int cpt=0;

		
		time1=System.nanoTime();
		ResultSet res = null ;
		prepareQuery("CREATE VIEW NodesDist1 AS SELECT edge.outNode from edge, node n1 where edge.inNode = n1.id and n1.name = ?");
		setInt(1,1);
		executeUpdate();
		prepareQuery("SELECT distinct res.name from node start, edge E1, edge E2, edge E3, node res" 
				+ " where start.name = ? and E1.inNode = start.name and E1.outNode = E2.inNode "
				+ " and E2.outNode = E3.inNode and E3.outNode = res.id"
				+ " and E1.outNode != E1.inNode and E3.outNode not in (select outNode from NodesDist1) and E3.outNode != E1.inNode"
				+ " and E1.weight > 50 and E2.weight > 50 and E3.weight > 50");
		for(int i = 1;i<=9999;i++){	
			setInt(1,testValues.get(i));		

			res = executeQuery();
			
			try {
				while(res.next()){
					System.out.println("Node: " + res.getInt("name"));
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		
		try {
			res.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		prepareQuery("DROP VIEW NodesDist1");
		executeUpdate();		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	protected void queryGetEdgeByPropertyGroupBy() {
		
		int cpt=0;

		time1=System.nanoTime();
		ResultSet res = null ;
		prepareQuery("SELECT count(*) as length,weight from edge where weight >= ? and weight <= ? group by weight;");
		for(int i = 0;i<testValues.size();i++){	
			setInt(1,testValues.get(i));
			setInt(2,testValues.get(i)+50);

			res = executeQuery();
			try {
				while(res.next()){

					cpt++;
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		try {
			res.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	protected void queryGetEdgeByProperty() {
		
		time1=System.nanoTime();
		int cpt=0;
		ResultSet res = null ;

		prepareQuery("SELECT * from edge where weight = ? limit 100"); // req est OK

		for(int i = 1;i<testValues.size();i++){

			setInt(1,testValues.get(i) );
			res = executeQuery();
			
			try {
				while(res.next()){
					
					cpt++;
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		try {
			res.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}
	
	

	protected void queryGetNodeByPropertyRangeGroupBy() {
		
		int cpt=0;

		
		ResultSet res = null ;

		time1=System.nanoTime();
		prepareQuery("SELECT count(*),age as length,age from node where age >= ? and age <= ? group by age;");
		for(int i = 1;i<testValues.size();i++){	
			setInt(1,testValues.get(i));
			setInt(2,testValues.get(i)+10);

			res = executeQuery();
			
			try {
				while(res.next()){
					cpt++;
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		try {
			res.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}


	protected void queryGetNeighborhoodFilter() {
		
		int cpt=0;

		
		ResultSet res = null ;

		time1=System.nanoTime();
		prepareQuery("SELECT n2.name from edge, node n1, node n2 "
				+ "                  where inNode = n1.name and outNode = n2.name and n1.name = ? and weight >= 200 and type = 'likes';");
		for(int i = 1;i<testValues.size();i++){	
			
			setInt(1,testValues.get(i));
			res = executeQuery();
			
			try {
				while(res.next()){
					cpt++;
				}


			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		try {
			res.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	protected void queryKHop() {
		int cpt=0;

		time1=System.nanoTime();

		
		System.out.println("queryGetKHop");
		ResultSet res = null ;
		//Tempory table : ne peut utiliser qu'une seule fois
		Statement statement = null;
		try {	
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for(int i = 1;i<testValues.size();i++){
			
			prepareQuery("CREATE TABLE IF not EXISTS NodesDist1 (node int, PRIMARY KEY (node)) ENGINE=MEMORY CHARSET=utf8;");
			executeUpdate();
			prepareQuery("INSERT INTO NodesDist1 (node) SELECT edge.outNode as nodes from edge where edge.inNode = ? and edge.outNode <> edge.inNode and edge.type = 'likes';");
			setInt(1, testValues.get(i));
			executeUpdate();
			prepareQuery("CREATE TABLE IF not EXISTS NodesDist2 (node int, PRIMARY KEY (node)) ENGINE=MEMORY CHARSET=utf8;");
			executeUpdate();
			prepareQuery("INSERT INTO NodesDist2 "
						+ "SELECT distinct E.outNode as nodes from NodesDist1, Edge E "
				   + "     where E.inNode = NodesDist1.node and E.outNode not in (select node from NodesDist1) and E.outNode <> ? and E.type = 'likes';");
			setInt(1, testValues.get(i));
			executeUpdate();
			
			
			prepareQuery("SELECT distinct n.name from NodesDist1, NodesDist2, Edge E, Node n" 
                    + " where E.inNode = NodesDist2.node and E.outNode not in (select node from NodesDist2) "
                    + " and E.outNode not in (select node from NodesDist1) and E.outNode <> ? and E.type = 'likes' and n.name = E.outNode limit 50" );
			setInt(1, testValues.get(i));
			res = executeQuery();			

			try {
				while(res.next()){
					cpt++;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}		
			
			
			prepareQuery("drop table if exists NodesDist1");
			executeUpdate();
			prepareQuery("drop table if exists NodesDist2");
			executeUpdate();
			
		}
	

		try {
			res.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
		
	}

	protected void executeUpdate() {
		try {
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}			
	}


	protected void queryGetEdgeByPropertyRange() {
		int cpt=0;

		time1=System.nanoTime();
		ResultSet res = null ;
		prepareQuery("SELECT * from edge where weight >= ? and weight <= ? limit 1000");

		for(int i = 1;i<testValues.size();i++){	
			setInt(1,testValues.get(i));
			setInt(2,testValues.get(i)+50);

			res = executeQuery();
			try {
				while(res.next()){
					cpt++;

				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		
		try {
			res.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);

	}

	protected void queryGetNodeByPropertyRange() {
		int cpt=0;

		ResultSet res = null ;

		time1=System.nanoTime();
		prepareQuery("SELECT * from node where age >= ? and age <= ? limit 500");
		for(int i = 1;i<testValues.size();i++){	
			setInt(1,testValues.get(i));
			setInt(2,testValues.get(i)+10);

			res = executeQuery();

			try {
				while(res.next()){
					cpt++;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		try {
			res.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}





	protected void queryDeleteNode() {
		
		time1=System.nanoTime();

		prepareQuery("delete from edge where inNode= ? or outNode= ? ;");

		for(int i = 1;i<=testValues.size();i++){	
			setInt(1,-i);
			setInt(2,-i);
		}
		prepareQuery("delete from node where name= ?;");
		for(int i = 1;i<=testValues.size();i++){	
			setInt(1,-i);
			updateQuery();
		}
		
		time2=System.nanoTime();
		double average = ((time2-time1)/Math.pow(10, 9));

		System.out.println(average);
	}

	protected void updateQuery() {
		try {
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	protected void queryDeleteEdge() {

		

		time1=System.nanoTime();

		prepareQuery("delete from edge where edge.inNode= ?");

		for(int i = 1;i<testValues.size();i++){	
			setInt(1,-i);

			updateQuery();

		}
		time2=System.nanoTime();
		double average = ((time2-time1)/Math.pow(10, 9));

		System.out.println(average);

	}





	protected void queryAddEdge() {
		time1=System.nanoTime();

		prepareQuery("insert into edge values (?, ?, ?,?)");

		for(int i = 1;i<testValues.size();i++){	
			setInt(1,-i); //In Node
			setInt(2,-i-1); //Out Node
			setInt(3,-1); // Weight
			setString(4,"likes"); // type
			updateQuery();

		}
		time2=System.nanoTime();
		double average = ((time2-time1)/Math.pow(10, 9));

		System.out.println(average);
		
	}

	protected void setString(int number, String value) {
		try {
			preparedStatement.setString(number, value);
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}

	protected void queryAddNode() {
		
				
		time1 = System.nanoTime();	
		prepareQuery("insert into node values (?, ?, default,default)");

		for(int i = 1;i<=testValues.size();i++){
			setInt(1,-i); //UserName
			setInt(2,-1); //Weight
			updateQuery();

		}
			
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		
	}


	protected void queryGetNeighborhood() {
		int cpt=0;

		
		ResultSet res = null ;

		time1=System.nanoTime();
		prepareQuery("SELECT n2.name from edge, node n1, node n2 "
				+ "                  where edge.inNode = n1.name and outNode = n2.name and n1.name = ? ;");
		
		for(int i = 1;i<testValues.size();i++){	
			
			setInt(1,testValues.get(i));
			res = executeQuery();
			
			try {
				while(res.next()){
					cpt++;
				}


			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		try {
			res.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);

	}



	protected void queryGetNodeByProperty() {
		
		ResultSet res = null ;
		int cpt=0;

		time1 = System.nanoTime();
		prepareQuery("SELECT name,age from node where name = ?;");
		for(int i = 1;i<testValues.size();i++){
			setInt(1,testValues.get(i) );
			res = executeQuery();
			try {
				while(res.next()){
					cpt++;
				}
	
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		try {
			res.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
		

	}

	protected ResultSet executeQuery() {
		try {
			ResultSet res = preparedStatement.executeQuery();
			return res;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}			
	}

	protected void setInt(int number, int value) {
		try {
			preparedStatement.setInt(number, value);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void setConnection(Connection connection) {
		this.connection= connection;
	}

	
	protected void prepareQuery(String sql) {
		
		try {
			if(preparedStatement != null){
				preparedStatement.close();
			}
			
			preparedStatement = connection.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}


	@Override
	protected void queryPageRank() {

		System.out.println("queryPageRank");
		time1=System.nanoTime();	
		
		prepareQuery("SET SQL_SAFE_UPDATES = 0;");
		executeUpdate();
		prepareQuery("CALL PageRank();");
		executeQuery();
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		
	}

	@Override
	protected void queryShortestPath() {
		
		int cpt=0;
		Integer node1 =null;
		Integer node2 = null;
		ResultSet res;
		SQLShortestPath shortest;
		int dist ;
		time1 = System.nanoTime();

		for (int i=0;i<testValues.size()-1;i++){

			prepareQuery("select n.name from node n where n.name = ?");
			setInt(1,testValues.get(i));
			 res = executeQuery();
			try {
				if(res.next()){
					node1 = res.getInt(1);
				}else{
					node1 = null;
				}
				setInt(1,testValues.get(i+1)); 
				 res = executeQuery();
				if(res.next()){
					node2 = res.getInt(1);
				}else{
					node2 = null;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			
			if(node1 != null && node2!= null ){
				shortest = new SQLShortestPath(connection,node1 ,node2);
				shortest.modeComplete(false);
				shortest.start();
				dist = shortest.getDistance();
				if(dist!=-1){
					cpt+=dist;
				}
			}
		}
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);	
		

		
	}

	@Override
	protected void queryUpdatePropertyWeight() {
		time1=System.nanoTime();
		prepareQuery("update edge set weight = ? where inNode = ? and outNode=?");

		for(int i = 1;i<testValues.size();i++){	
			setInt(1,-1); //In Node

			setInt(2,-i); //In Node
			setInt(3,-i-1); //Out Node
			updateQuery();

		}
		time2=System.nanoTime();
		double average = ((time2-time1)/Math.pow(10, 9));

		System.out.println(average);		
	}

	@Override
	protected void queryShortestPathWeight() {
		

		int cpt=0;
		Integer node1 =null;
		Integer node2 = null;
		ResultSet res;
		SQLShortestPath shortest;
		int dist ;
		time1 = System.nanoTime();

		for (int i=0;i<testValues.size()-1;i++){

			prepareQuery("select n.name from node n where n.name = ?");
			setInt(1,testValues.get(i));
			 res = executeQuery();
			try {
				if(res.next()){
					node1 = res.getInt(1);
				}else{
					node1 = null;
				}
				setInt(1,testValues.get(i+1)); 
				 res = executeQuery();
				if(res.next()){
					node2 = res.getInt(1);
				}else{
					node2 = null;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			
			if(node1 != null && node2!= null ){
				shortest = new SQLShortestPath(connection,node1 ,node2);
				shortest.modeComplete(true);
				shortest.start();
				dist = shortest.getDistance();
				if(dist!=-1){
					cpt+=dist;
				}
			}
		}
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);	

	}


}
