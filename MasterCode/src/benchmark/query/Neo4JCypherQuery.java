package benchmark.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;

import benchmark.Neo4JBenchmark;
import lib.Query;


//Note: a-->b peut être les deux mêmes!

public class Neo4JCypherQuery extends Query {

	protected GraphDatabaseService graphDb;
	protected Label labelPerson = Neo4JBenchmark.labelPerson;
	protected long time1;
	protected long time2;

	public Neo4JCypherQuery(GraphDatabaseService graphDb) {
		this.graphDb = graphDb;
		
	}

	public void start(ArrayList<Integer> testValues) {
		super.start(testValues);
		
		//printGraph();
		
	
//		 try (Transaction tx = graphDb.beginTx()) {
//			 System.out.println("Nombre de noeuds : "+IteratorUtil.count(GlobalGraphOperations.at(graphDb).getAllNodes()));
//			 System.out.println("Nombre d'arcs  : " + IteratorUtil.count(GlobalGraphOperations.at(graphDb).getAllRelationships()));
//		 }
		System.out.println("Neo4J : Fin des Tests");

	}

	protected void queryGraphMatchingConditionnal() {
		Map<String, Object> params = new HashMap<String, Object>();
		
		Result result;
		int cpt=0;
		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {
			for(int i =0;i<testValues.size();i++){
				params.put( "name", testValues.get(i));
			
				result = graphDb.execute("MATCH (a:Person)-[r1:Likes]->(b:Person)-[:Hates]->(c:Person), (a:Person)-[r2:Likes]->(c:Person) "
						+ "where a<>b and c<>b and c<>a and a.name={name} and b.age > 18 and c.age > 18"
						+ " and r1.weight < 400 and r2.weight < 400"
						+ " return a,b,c",params);

			
				while (result.hasNext()) {
//					Map<String, Object> row = result.next(); // influence pas res
//					for (Entry<String, Object> column : row.entrySet()) {
//						column.getKey();
//						column.getValue();
//					}
					result.next();
					cpt++;
				}
			}
		}	
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	
	protected void queryAddEdge() {
		
		Map<String, Object> props = new HashMap<String, Object>();

		Iterator<Node> toDeleteIt;
		ArrayList<Node> toDelete =new ArrayList<Node>();
		int cpt=0;
		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {

			for(int i = 1;i<testValues.size();i++){	
				toDelete.add( graphDb.findNode(labelPerson, "name", -i));
				toDelete.add( graphDb.findNode(labelPerson, "name", -i-1));
			}
			
			toDeleteIt = toDelete.iterator();
			
			time1=System.nanoTime();
			while(toDeleteIt.hasNext()){
				props.put( "idA", toDeleteIt.next().getId() );
				props.put( "idB", toDeleteIt.next().getId() );
				props.put( "weight", -1 );

				graphDb.execute("MATCH (a:Person),(b:Person) WHERE id(a) = {idA} AND id(b) = {idB} CREATE (a)-[r:Likes{weight : {weight}}]->(b)",props);
			}	
			tx.success();
		}
		time2=System.nanoTime();
		double average = ((time2-time1)/Math.pow(10, 9));
		System.out.println(average);
	}

	protected void printGraph() {
		try (Transaction tx = graphDb.beginTx()) {

		for (Node n : graphDb.getAllNodes()) {
			System.out.print(n + ":");
			for (String key : n.getPropertyKeys())
				System.out.print(key + "=" + n.getProperty(key) + ", ");
			System.out.println();
		}

		for (Relationship r : graphDb.getAllRelationships()) {
			Node n1 = r.getStartNode();
			Node n2 = r.getEndNode();
			System.out.print(n1.getProperty("name") + " -"+r.getType().name()+"-> " + n2.getProperty("name") + " (" + r.getId() + ") ");
			for (String key : r.getPropertyKeys())
				System.out.print(key + "=" + r.getProperty(key) + ", ");

			System.out.println();
		}
		}


	}

	

	protected void queryKHop() {

		Map<String, Object> params = new HashMap<String, Object>();
		
		Result result;
		String rows = "";
		int cpt=0;
		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {
			for(int i =1;i<testValues.size();i++){
				params.put( "name", testValues.get(i));
	
			result = graphDb.execute("MATCH (a:Person{name:{name}})-[:Likes]->(b:Person) "
					+ "WITH a, filter(n in collect(b) where n <> a) AS nodesDist1 "
					+ "UNWIND nodesDist1 AS b1 "
					+ "MATCH (b1:Person)-[:Likes]->(c:Person) "
					+ "WITH a, nodesDist1, filter(n in collect(distinct c) where not n in nodesDist1 and n <> a) AS nodesDist2 "
					+ "UNWIND nodesDist2 AS c1 "
					+ "MATCH (c1:Person)-[:Likes]->(d:Person) "
					+ "WITH a, nodesDist1, nodesDist2, filter(n in collect(distinct d) where not n in nodesDist1 and not n in nodesDist2 and n <> a ) AS nodesDist3 "
					+ "UNWIND nodesDist3 AS d1 RETURN d1.name limit 50",params); //Compte tous les combi ex b->a1, b->a2 donc besoin distinct
		

			
			while (result.hasNext()) {

				result.next();
				cpt++;
			}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	protected void queryGetEdgeByProperty() {
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		Result result;
		String rows = "";
		int cpt=0;
		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {
			for(int i =0;i<testValues.size();i++){
				params.put( "weight", testValues.get(i));
				

				result = graphDb.execute("match (a:Person)-[r]->(b) where r.weight = {weight} return r limit 100 ",params); // return n, n.name

				while (result.hasNext()) {

					result.next();
					
					cpt++;
				}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	protected void queryGetEdgeByPropertyGroupBy() {
		Map<String, Object> params = new HashMap<String, Object>();

		Result result;
		String rows = "";
		int cpt=0;
		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {

			for(int i =0;i<testValues.size();i++){
				params.put( "min", testValues.get(i));
				params.put( "max", testValues.get(i)+50);

				result = graphDb.execute(
					"match (a:Person)-[r]->(b) where r.weight >= {min} and r.weight <= {max} return r.weight, COUNT(r.weight) AS length ",params); // return
																															// n,
																															// n.name

			while (result.hasNext()) {
				result.next();
				cpt++;
			}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);

	}

	protected void queryGetNodeByPropertyRangeGroupBy() {
		Map<String, Object> params = new HashMap<String, Object>();


		Result result;
		String rows = "";
		int cpt=0;
		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {
			for(int i =0;i<testValues.size();i++){
				params.put( "min", testValues.get(i));
				params.put( "max", testValues.get(i));

				result = graphDb.execute("match (n:Person) where n.age >= {min} and n.age <= {max} return n.age, COUNT(n.age) AS length ",params); // n.name est
																													// le grouping
																													// key

			while (result.hasNext()) {
				result.next();
				cpt++;
				
			}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	protected void queryGetNeighborhoodFilter() {
		Map<String, Object> params = new HashMap<String, Object>();
		
		Result result;
		String rows = "";
		int cpt=0;
		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {
			for(int i =1;i<testValues.size();i++){
				params.put( "name", testValues.get(i));
		

				result = graphDb.execute("MATCH (a:Person {name: {name}})-[r:Likes]->(b) where r.weight >= 200 RETURN b",params); // return n, n.name

				while(result.hasNext()){
					result.next();
					cpt++;
				}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	protected void queryUpdateProperty() {
		Map<String, Object> params = new HashMap<String, Object>();
		
		ArrayList<Node> toDelete1 =new ArrayList<Node>();
		int cpt=0;
		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {

			for(int i = 1;i<=testValues.size();i++){ 
				toDelete1.add( graphDb.findNode(labelPerson, "name", -i));
			}
			
			time1=System.nanoTime();
			for(int i = 0;i<testValues.size();i++){ //on part de 0 car on parcourt la liste
				
				params.put( "id", toDelete1.get(i).getId() );
				params.put( "newAge", 18 );

				graphDb.execute("MATCH (n:Person) where id(n) = {id} SET n.age = {newAge}",params);

			}
			tx.success();
		}
		
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
	}




	protected void queryDeleteEdge() {
		
		Map<String, Object> params = new HashMap<String, Object>();

		Iterator<Relationship> toDeleteIt;
		
		ArrayList<Relationship> toDelete1 = new ArrayList<Relationship>();
		try (Transaction tx = graphDb.beginTx()) {
	
			for(int i = 1;i<testValues.size();i++){
				toDelete1.add(graphDb.findNode(labelPerson, "name", -i).getRelationships(Direction.OUTGOING).iterator().next());
			}
			toDeleteIt = toDelete1.iterator();
			
			time1=System.nanoTime();
			
			while(toDeleteIt.hasNext()){
				params.put( "id", toDeleteIt.next().getId());

				graphDb.execute("MATCH (a:Person)-[r]->(b) WHERE id(r) = {id} DETACH DELETE r",params);

			}
			tx.success();
		}
		time2=System.nanoTime();
		double average = (time2-time1)/Math.pow(10, 9);
		System.out.println(average);

	}

	protected void queryDeleteNode() {
	
		//LA suppresion du noeud vire aussi ses arrêtes
		Map<String, Object> params = new HashMap<String, Object>();
	
		ArrayList<Node> toDelete1 =new ArrayList<Node>();
		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {

			for(int i = 1;i<=testValues.size();i++){
				toDelete1.add(graphDb.findNode(labelPerson, "name", -i));	
			}
			
			Iterator<Node> toDeleteIt = toDelete1.iterator();
			
			time1=System.nanoTime();
			while(toDeleteIt.hasNext()){
				params.put( "id", toDeleteIt.next().getId()); 
				graphDb.execute("MATCH (n:Person) where id(n)={id} DETACH DELETE n",params);
			}		
			tx.success();
		}		
		time2=System.nanoTime();
		double average = ((time2-time1)/Math.pow(10, 9));
		System.out.println(average);

	}



	protected void queryAddNode() {
	
		Map<String, Object> props = new HashMap<String, Object>();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put( "props", props );
		int cpt=0;
		time1 = System.nanoTime();	
		try (Transaction tx = graphDb.beginTx()) {
			for(int i = 1;i<=testValues.size();i++){
				props.put( "name", -i );
				props.put( "age", -1 );
				params.put( "props", props );
				graphDb.execute("CREATE (n:Person {props})",params);
			}
			tx.success();
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
	}

	protected void queryGetNeighborhood() {

		Map<String, Object> params = new HashMap<String, Object>();
		
		Result result;
		String rows = "";
		int cpt=0;
		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {
			for(int i =1;i<testValues.size();i++){
				params.put( "name", testValues.get(i));
		

				result = graphDb.execute("MATCH (a:Person {name: {name}})-->(b:Person) RETURN b",params); // return n, n.name

				while(result.hasNext()){
					result.next();
					cpt++;
				}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);

	}


	protected void queryGetNodeByProperty() {
		Map<String, Object> params = new HashMap<String, Object>();
		
		Result result;
		String rows = "";
		int cpt=0;
		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {
			for(int i =1;i<testValues.size();i++){
			params.put( "name", testValues.get(i));

			result = graphDb.execute("profile match (n:Person {name: {name}}) return n.name,n.age",params); // shortcut return n, n.name

			while(result.hasNext()){
				result.next();
				cpt++;
			}

		}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}


	protected void queryGetEdgeByPropertyRange() {
		int cpt=0;
		time1 = System.nanoTime();
		Map<String, Object> params = new HashMap<String, Object>();
		
		Result result;
		String rows = "";
		try (Transaction tx = graphDb.beginTx()) {
			for(int i =0;i<testValues.size();i++){
				params.put( "min", testValues.get(i));
				params.put( "max", testValues.get(i)+50);

			result = graphDb.execute("match (a:Person)-[r]->(b) where r.weight >= {min} and r.weight <= {max} return r limit 500",params); // return n, n.name

			while (result.hasNext()) {

				result.next();
				cpt++;
			}
			}
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);

	}

	protected void queryGetNodeByPropertyRange() {
		int cpt=0;
		time1 = System.nanoTime();
		Map<String, Object> params = new HashMap<String, Object>();

		Result result;
		try (Transaction tx = graphDb.beginTx()) {
			for(int i =1;i<testValues.size();i++){
				params.put( "min", testValues.get(i));
				params.put( "max", testValues.get(i)+10);

			result = graphDb.execute("match (n:Person) where n.age >= {min} and n.age <= {max} return n limit 500",params); // return n, n.name

			while(result.hasNext()){
				result.next();
				cpt++;
			}
				
			}
			
		}
		time2=System.nanoTime();
		System.out.println("Temps écoule"+(time2-time1)/Math.pow(10, 9));
		System.out.println(cpt);
	}

	@Override
	protected void queryPageRank() {
		
	}



	@Override
	protected void queryUpdatePropertyWeight() {
		Map<String, Object> props = new HashMap<String, Object>();

		Iterator<Node> toDeleteIt;
		ArrayList<Node> toDelete =new ArrayList<Node>();
		int cpt=0;
		time1 = System.nanoTime();
		try (Transaction tx = graphDb.beginTx()) {

			for(int i = 1;i<testValues.size();i++){	
				toDelete.add( graphDb.findNode(labelPerson, "name", -i));
				toDelete.add( graphDb.findNode(labelPerson, "name", -i-1));
			}
			
			toDeleteIt = toDelete.iterator();
			
			time1=System.nanoTime();
			while(toDeleteIt.hasNext()){
				props.put( "id", toDeleteIt.next().getId() );
				props.put( "weight", -1 );

				graphDb.execute("MATCH (a:Person)-[r]->(b) where id(r) = {id} SET r.weight = {weight}",props);
			}	
			tx.success();
		}
		time2=System.nanoTime();
		double average = ((time2-time1)/Math.pow(10, 9));
		System.out.println(average);		
	}

	@Override
	protected void queryShortestPathWeight() {
		
	}

	@Override
	protected void queryShortestPath() {
		
	}




}
