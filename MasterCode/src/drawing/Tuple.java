package drawing;

public class Tuple{
	public String x;
	public double y;

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Tuple(String x, double estimatedTime) {
		this.x = x;
		this.y = estimatedTime;
	}
}