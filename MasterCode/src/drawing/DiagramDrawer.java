package drawing;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.Shape;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeriesCollection;

public class DiagramDrawer {

	JFreeChart chart;
	String xAxis;
	String yAxis;
	XYSeriesCollection dataset;
	String subtitle = "";
	private String outputName;
	DefaultCategoryDataset defaultcategorydataset;
	ArrayList<	ArrayList<Tuple> > rest = new ArrayList<ArrayList<Tuple> >();
	ArrayList<String> restName = new ArrayList<String>();
	private boolean first=true;
	private String titleString;

	public void draw() {
		
		
	
		
		chart = ChartFactory.createLineChart("Resultat", xAxis, yAxis, defaultcategorydataset, PlotOrientation.VERTICAL,
				true, true, false);
	
		
		TextTitle subtitleTT = new TextTitle(subtitle, new Font("SansSerif", Font.BOLD, 25));
		chart.addSubtitle(subtitleTT);

		final TextTitle title = new TextTitle(titleString, new Font("SansSerif", Font.BOLD, 35));
		chart.setTitle(title);


		CategoryPlot categoryplot = (CategoryPlot) chart.getPlot();
		
		
		categoryplot.setRenderer(new LineAndShapeRenderer(){
         public Shape lookupLegendShape(int series){
            return new Rectangle(15, 15);
         }
		});

		//AXE Y
		NumberAxis numberaxis = (NumberAxis) categoryplot.getRangeAxis();
		numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		numberaxis.setUpperMargin(0.14999999999999999D); // sinon les label du haut mangé
		Font font = new Font("Dialog", Font.PLAIN, 30);
		numberaxis.setTickLabelFont(font);
		numberaxis.setLabelFont(font);

		//Chiffre de AXE X
		categoryplot.getDomainAxis().setTickLabelFont(font);
		categoryplot.getDomainAxis().setLabelFont(font);
		
		//if(outputName.equals("getNode"))
		//	numberaxis.setTickUnit(new NumberTickUnit(0.1)); // si met 1 trop serré
		if(outputName.equals("addArc"))
			numberaxis.setUpperBound(150);
		if(outputName.equals("getNodeRange"))
			numberaxis.setUpperBound(25);
		if(outputName.equals("khop"))
			numberaxis.setUpperBound(60);
		if(outputName.equals("majArcs"))
			numberaxis.setUpperBound(70);
		
		LineAndShapeRenderer lineandshaperenderer = (LineAndShapeRenderer) categoryplot.getRenderer();
		lineandshaperenderer.setSeriesPaint(0, new Color(0xc0, 0x00, 0x00));//dark red
		//lineandshaperenderer.setBaseShapesVisible(true);// montre les boules pour chaque donnée de la ligne
		//lineandshaperenderer.setDrawOutlines(true); // doit être lié au label
		//lineandshaperenderer.setUseFillPaint(true); // fille les shape
		//lineandshaperenderer.setBaseFillPaint(Color.white); // fait rien
		
		
		//MAJ LEGENCE
		lineandshaperenderer.setBaseLegendTextFont(new Font("Helvetica", Font.BOLD, 25));
		decideColor(lineandshaperenderer,defaultcategorydataset.getRowKey(0).toString());


		categoryplot.setBackgroundPaint(java.awt.Color.WHITE); // fond écran
		categoryplot.setDomainGridlinePaint(java.awt.Color.GRAY); // grilles verticales
		categoryplot.setRangeGridlinePaint(java.awt.Color.GRAY); // grilles horizontales
		
		DefaultCategoryDataset categorydataset = null;
		LineAndShapeRenderer lineandshaperenderer2 =null;
		for(int i=0;i<rest.size();i++){
			categorydataset = new DefaultCategoryDataset();
			for(int j=0;j<rest.get(i).size();j++){		
				if(rest.get(i).get(j).getY() == -1)
					categorydataset.addValue(null, restName.get(i), String.valueOf(rest.get(i).get(j).getX()));//y / x
				else
					categorydataset.addValue(rest.get(i).get(j).getY(), restName.get(i), String.valueOf(rest.get(i).get(j).getX()));//y / x
			}

			categoryplot.setDataset(i+1, categorydataset); //Pas besoin de définir range axis sinon ca crée un doublon
			categoryplot.mapDatasetToRangeAxis(i+1, 0);
			categoryplot.mapDatasetToDomainAxis(i+1,0);
			
			lineandshaperenderer2 = new LineAndShapeRenderer(){
		         public Shape lookupLegendShape(int series){
		             return new Rectangle(15, 15);
		          }
		 		}; // besoin de mon propre render
		 		
			lineandshaperenderer2.setBaseShapesVisible(true);// montre les boules pour chaque donnée de la ligne
			lineandshaperenderer2.setDrawOutlines(true); // doit être lié au label
			lineandshaperenderer2.setUseFillPaint(false); // fille les shape
			lineandshaperenderer2.setBaseFillPaint(Color.white); // fait rien
			decideColor(lineandshaperenderer2,restName.get(i));
			

			
			lineandshaperenderer2.setSeriesStroke(0, new BasicStroke(2.5f));
			lineandshaperenderer2.setBaseLegendTextFont(new Font("Helvetica", Font.BOLD, 25));

			categoryplot.setRenderer(i+1, lineandshaperenderer2);		
		}
		categoryplot.getRenderer().setSeriesStroke(0, new BasicStroke(2.5f));
		
		

	
		
		try {
			ChartUtilities.writeChartAsPNG(
					new FileOutputStream(new File("").getAbsolutePath() + File.separator + "draw" + File.separator + "pseudoRandom" + File.separator+  outputName + ".png"),
					chart, 900, 700); //largeur, hauteur

//			// Avoir les labels sur graphique
//			lineandshaperenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
//			lineandshaperenderer.setBaseItemLabelsVisible(true);
//			ChartUtilities.writeChartAsPNG(
//					new FileOutputStream(new File("").getAbsolutePath() + File.separator + outputName + "LABELS.png"),
//					chart, 1440, 960);
//			ChartUtilities.writeChartAsPNG(
//					new FileOutputStream(new File("").getAbsolutePath() + File.separator + outputName + "LABELS.png"),
//					chart, 1440, 960);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void decideColor(LineAndShapeRenderer lineandshaperenderer, String name) {
		
		if(name.equals("MySQL")) //remplace le jaune par du noir
			lineandshaperenderer.setSeriesPaint(0, Color.BLUE); //dark green
		if(name.equals("Neo4J")) //remplace le jaune par du noir
			lineandshaperenderer.setSeriesPaint(0, new Color(0x00, 0x80, 0x00)); //dark green
		if(name.equals("Cypher")) //remplace le jaune par du vert flash
			lineandshaperenderer.setSeriesPaint(0, new Color(85, 255, 0));
		if(name.equals("OSQL")) //remplace le jaune par du rouge clair
			lineandshaperenderer.setSeriesPaint(0, new Color(255, 80, 80));
		if(name.equals("Titan")) //remplace le jaune par du noir
			lineandshaperenderer.setSeriesPaint(0, Color.GRAY);
		if(name.equals("MySQL (Native)")) //remplace le jaune par du noir
			lineandshaperenderer.setSeriesPaint(0, new Color(64, 153, 255));		
		if(name.equals("MySQL (Native)")) //remplace le jaune par du noir
			lineandshaperenderer.setSeriesPaint(0, Color.red);	
	}

	public void setAxis(String xAxis, String yAxis) {
		this.xAxis = xAxis;
		this.yAxis = yAxis;
	}

	public void addLine(ArrayList<Tuple> list, String serieName) {

		PrintWriter writer = null;
		try {
			writer = new PrintWriter(outputName + ".txt", "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		System.out.println(serieName);
		
		String temp;
		if(first){
			
			for (int i = 0; i < list.size(); i++) {
				temp = list.get(i).getX() + " " + list.get(i).getY();
				//System.out.println(temp);
				writer.println(temp);
				if(list.get(i).getY() == -1)
					defaultcategorydataset.addValue(null, serieName, list.get(i).getX());//y / x
				else
					defaultcategorydataset.addValue(list.get(i).getY(), serieName, list.get(i).getX()); // y, serie, x
	
			}
			first = false;
		}else{
			//System.out.println("reste");
			for (int i = 0; i < list.size(); i++) {
				temp = list.get(i).getX() + " " + list.get(i).getY();
				//System.out.println(temp);
				writer.println(temp);

			}
			rest.add(list);
			restName.add(serieName);

		
		}
		writer.close();

	}

	public void setSubTitle(String subtitle) {
		this.subtitle = subtitle;

	}
	public void setTitle(String title) {
		this.titleString = title;

	}

	public void setOutputName(String outputName) {
		this.outputName = outputName;

	}

	public DiagramDrawer() {
		defaultcategorydataset = new DefaultCategoryDataset();
	}
}