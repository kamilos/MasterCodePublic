package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;


import benchmark.MysqlBenchmark;
import benchmark.Neo4JBenchmark;
import benchmark.OrientDBBenchmark;
import benchmark.TitanBenchmark;
import constant.Const;
import constant.Const.CHOICES;
import drawing.DiagramDrawer;
import drawing.Tuple;
import lib.Benchmark;
import lib.NumbersGenerator;
import parser.GraphParser;

// Note : -Xmx6g alloue 6GB de mémoire



//Note il n'y a pas de 1-likes->2 et simultanément 1-hates->2
public class App {
	private static long time1;
	private static long time2;
	static GraphParser parser;
	public static void main(String[] args) throws Exception {
		
		
		System.out.println("Master Code");
		boolean draw = true;
		if(false){
			try{
				 draw();
			 }catch(Exception ex){ex.printStackTrace();}
			
		}else{
		

		 
		if (args[0].split("=")[1].equals("false")) {
			Const.INIT = false;
		} else {
			Const.INIT = true;
		}
		


		 parser = new GraphParser();
		if (Const.INIT) {
			
			
			parser.parse2(Const.DATASET_PATH);
			System.out.println("chargement de 2 ^"+parser.NbNodes);
			if(Const.NEW){
				new NumbersGenerator().generateAgeANDWeight(parser.NbNodes, parser.NbEdges);
			}
			parser.loadWeight();
			parser.loadAge();		
		}
		if(Const.DB_CHOICE == Const.DB_CHOICES.AUTO){
			autoMode();
			
		}else{
			if(Const.CHOICE== Const.CHOICES.SPECIAL){
				new NumbersGenerator().generateTestValues(100000,1,2); // number, min, max
				
			}else if(Const.CHOICE== Const.CHOICES.GetNodeByPropertyRange ){
				new NumbersGenerator().generateTestValues(1000,1,100); // number, min, max
			}else if(Const.CHOICE== Const.CHOICES.PageRankNative || Const.CHOICE== Const.CHOICES.PageRank ){
					new NumbersGenerator().generateTestValues(2,1,2); // number, min, max
			}else if(Const.CHOICE== Const.CHOICES.GraphMatchingConditionnal ){

				new NumbersGenerator().generateTestValues(1000,1,(int) Math.pow(2,Double.valueOf(Const.SIZE.split("_")[1]))); // number, min, max
							
			}else if(Const.CHOICE== Const.CHOICES.GetNodeByPropertyRangeGroupBy ){
				new NumbersGenerator().generateTestValues(100,1,100); // number, min, max
			}else if(Const.CHOICE== Const.CHOICES.GetEdgeByProperty || Const.CHOICE== Const.CHOICES.GetEdgeByPropertyRange  ){
				new NumbersGenerator().generateTestValues(1000,1,500); // number, min, max
	
			}else if(Const.CHOICE== Const.CHOICES.GetEdgeByPropertyGroupBy ){
				if(Const.NEW){
					new NumbersGenerator().generateTestValues(100,1,500); // number, min, max
				}
			}else if(Const.CHOICE== Const.CHOICES.ShortestPath || Const.CHOICE== Const.CHOICES.ShortestPathWeight){
				//double upper = 20;
				double upper = Double.valueOf(Const.SIZE.split("_")[1]);

				if(Const.NEW){
					new NumbersGenerator().generateTestValues(201,1,(int) Math.pow(2, upper)); // number, min, max
				}
			}else if(Const.CHOICE== Const.CHOICES.GetKhop){
				//double upper = 19;
				double upper = Double.valueOf(Const.SIZE.split("_")[1]);
				if(Const.NEW){
					new NumbersGenerator().generateTestValues(1000,1,(int) Math.pow(2, upper)); // number, min, max
				}
			}else{
				if(Const.NEW){
					//double upper = Double.valueOf(Const.SIZE.split("_")[1]);
					double upper = 19;
					new NumbersGenerator().generateTestValues(10000,1,(int) Math.pow(2,upper )); // number, min, max
				}
			}
			
			parser.loadTestValues();
			
			if(Const.CHOICE != Const.CHOICES.SPECIAL || Const.INIT == true){
				time1 = System.nanoTime();
				Benchmark bench = null;
				if(Const.DB_CHOICE == Const.DB_CHOICES.ORIENTDB|| Const.DB_CHOICE == Const.DB_CHOICES.ORIENTDBOSQL){
					bench = new OrientDBBenchmark();
				}else if(Const.DB_CHOICE == Const.DB_CHOICES.TITAN){
					bench= new TitanBenchmark();
				}else if(Const.DB_CHOICE == Const.DB_CHOICES.MYSQL || Const.DB_CHOICE == Const.DB_CHOICES.MYSQL_NATIVE){
					bench= new MysqlBenchmark();
				}else if(Const.DB_CHOICE == Const.DB_CHOICES.NEO4J || Const.DB_CHOICE == Const.DB_CHOICES.NEO4J_CYPHER){
					bench= new Neo4JBenchmark();
				}
				
				 bench.setData(parser.nodes,parser.edges);
				 bench.setData2(parser.nodesEdges);
				 bench.setWeight(parser.weight);
				 bench.setAge(parser.age);
				 bench.setTestValues(parser.testValues);
				 
				if(Const.DB_CHOICE == Const.DB_CHOICES.NEO4J_CYPHER){
					((Neo4JBenchmark) bench).initCypherMode();
				}else{
					bench.init();
				}
				if(Const.INIT){
					time2=System.nanoTime();
					System.out.println(((time2-time1)/Math.pow(10, 9)));
				}
			}else{
				special();
			}
		}
		}

		 
		System.out.println("-FIN-");
		System.exit(0);
		
		

	}



	private static void autoMode() {
		BufferedReader br;
		int mode=-1;

		try {
			br = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(Const.AUTO_PATH))));
			mode = Integer.valueOf(br.readLine());		
			br.close();	
		} catch (Exception e1) {
			e1.printStackTrace();
		}
			
		
		//mode 0 = OrientDB, 1 = OSQL, 2 = Titan, 3 = Mysql, 4 NeoCypher, 5 NeoNative, 6 fini;
		 Path source = Paths.get(new File("").getAbsolutePath() + File.separator + "pom1.xml");

		double upper = Double.valueOf(Const.SIZE.split("_")[1]);
		//double upper = 20;
		switch (mode)
		{
		  case 0: //1er fois on init
			  	Const.DB_CHOICE = Const.DB_CHOICES.ORIENTDB;
			  
			  	if(Const.CHOICE== Const.CHOICES.GetNodeByPropertyRange  ){
			  		new NumbersGenerator().generateTestValues(1000,1,100); // number, min, max
			  	}else if(Const.CHOICE== Const.CHOICES.PageRankNative ){
					new NumbersGenerator().generateTestValues(10,1,2); // number, min, max
					
			  	}else if(Const.CHOICE== Const.CHOICES.GetNodeByPropertyRangeGroupBy 
			  			|| Const.CHOICE== Const.CHOICES.GetNodeByPropertyRangeNoLimit ){
			  		new NumbersGenerator().generateTestValues(100,1,100); // number, min, max
					
				}else if(Const.CHOICE== Const.CHOICES.GetKhop 
						|| Const.CHOICE== Const.CHOICES.GraphMatchingConditionnal  ){
					new NumbersGenerator().generateTestValues(1000,1,(int) Math.pow(2,upper)); // number, min, max
							
				}else if( Const.CHOICE== Const.CHOICES.GetEdgeByPropertyGroupBy ){
						new NumbersGenerator().generateTestValues(51,1,500); // number, min, max					
				}else if( Const.CHOICE== Const.CHOICES.GetEdgeByPropertyRange){
					new NumbersGenerator().generateTestValues(1000,1,500); // number, min, max					
				}
				else if(Const.CHOICE== Const.CHOICES.ShortestPath || Const.CHOICE== Const.CHOICES.ShortestPathWeight){
					new NumbersGenerator().generateTestValues(51,1,(int) Math.pow(2, upper)); // number, min, max
					
				}else{
					new NumbersGenerator().generateTestValues(10000,1,(int) Math.pow(2, upper)); // number, min, max
				}
			  break;  
		  case 1:
			  Const.DB_CHOICE = Const.DB_CHOICES.ORIENTDBOSQL;
			  source = Paths.get(new File("").getAbsolutePath() + File.separator + "pom1.xml");
				 try {
					Files.move(source, source.resolveSibling("pom2.xml"));
					source = Paths.get(new File("").getAbsolutePath() + File.separator + "pom.xml");
					 Files.move(source, source.resolveSibling("pom1.xml"));
					 source = Paths.get(new File("").getAbsolutePath() + File.separator + "pom2.xml");
	
					 Files.move(source, source.resolveSibling("pom.xml"));
				 } catch (IOException e) {
						e.printStackTrace();
					}
			    break;
		  case 2:
			  Const.DB_CHOICE = Const.DB_CHOICES.TITAN;
			    break;
		  case 3:
			  Const.DB_CHOICE = Const.DB_CHOICES.MYSQL;
			  System.err.println("PROCHAIN NEO !");
			  source = Paths.get(new File("").getAbsolutePath() + File.separator + "pom1.xml");
			 try {
				Files.move(source, source.resolveSibling("pom2.xml"));
				source = Paths.get(new File("").getAbsolutePath() + File.separator + "pom.xml");
				 Files.move(source, source.resolveSibling("pom1.xml"));
				 source = Paths.get(new File("").getAbsolutePath() + File.separator + "pom2.xml");

				 Files.move(source, source.resolveSibling("pom.xml"));
				} catch (IOException e) {
					e.printStackTrace();
				}
			    break;
		  case 4:
			  Const.DB_CHOICE = Const.DB_CHOICES.NEO4J_CYPHER;
			    break;
		  case 5:
			  Const.DB_CHOICE = Const.DB_CHOICES.NEO4J;
			    break;
		  case 6:
			  	//Const.DB_CHOICE = Const.DB_CHOICES.MYSQL_NATIVE;

			  	System.out.println("Fin du cycle !");
			    break;
		  default:
			  break;             
		}
		
		try {
			parser.loadTestValues();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		

		
		Benchmark bench = null;
		if(Const.DB_CHOICE == Const.DB_CHOICES.ORIENTDB|| Const.DB_CHOICE == Const.DB_CHOICES.ORIENTDBOSQL){
			bench = new OrientDBBenchmark();
		}else if(Const.DB_CHOICE == Const.DB_CHOICES.TITAN){
			bench= new TitanBenchmark();
		}else if(Const.DB_CHOICE == Const.DB_CHOICES.MYSQL || Const.DB_CHOICE == Const.DB_CHOICES.MYSQL_NATIVE){
			//bench= new MysqlBenchmark();
		}else if(Const.DB_CHOICE == Const.DB_CHOICES.NEO4J || Const.DB_CHOICE == Const.DB_CHOICES.NEO4J_CYPHER){
			bench= new Neo4JBenchmark();
		}
		if(bench != null){
			 bench.setData(parser.nodes,parser.edges);
			 bench.setData2(parser.nodesEdges);
			 bench.setWeight(parser.weight);
			 bench.setAge(parser.age);
			 bench.setTestValues(parser.testValues);
			 
			 if(Const.DB_CHOICE == Const.DB_CHOICES.NEO4J_CYPHER){
					((Neo4JBenchmark) bench).initCypherMode();
				}else{
					bench.init();
				}
		}
		 
		

		mode++;
		try {
			FileWriter fw = new FileWriter(new File(Const.AUTO_PATH));
			BufferedWriter bw = new BufferedWriter(fw);
			if(mode!=7)
				bw.write(String.valueOf(mode));
			else
				bw.write("0");
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



	private static void draw() throws Exception {
		



		ArrayList<String> fileNames = new ArrayList<String>();
	
		for(int i = 0;i<fileNames.size();i++){
			System.out.println(fileNames.get(i));
		    String fileName = fileNames.get(i);
	
			BufferedReader br = new BufferedReader(new FileReader(new File("").getAbsolutePath() + File.separator + "draw" 
			                      + File.separator + "pseudoRandom" + File.separator+ fileName+".txt"));
	
		    Map<String, ArrayList<Tuple>> myMap = new HashMap<String, ArrayList<Tuple>>();
		    String title;
		    String subtitle;
		    String yaxisName;
			try {
	
			    String line;
			    String[] content;
			    title = br.readLine();
			    subtitle = br.readLine();
			    yaxisName = br.readLine();
	
				while ((line = br.readLine()) != null) {
					if(line.length()>1 && !line.startsWith("//")){
				        content = line.split(";");
				        
				        if(!myMap.containsKey(content[0].trim())){ // trim vire les espace vide apres neo4
				        	myMap.put(content[0].trim(), new ArrayList<Tuple>());
				        }
				        
				        myMap.get(content[0].trim()).add(new Tuple(content[1].trim(), Double.valueOf(content[2].split("//")[0].trim())));
				      
					}	
			        
			    }
			} finally {
			    br.close();
			}
	
			DiagramDrawer graph = new DiagramDrawer();
			if(yaxisName.equals("Itérations")){
				graph.setAxis(yaxisName, "Noeuds");
	
			}else{
				graph.setAxis("Taille du graphe", yaxisName);
			}
			graph.setTitle(title);
			if(subtitle.equals("rien"))
				graph.setSubTitle("");
			else
				graph.setSubTitle(subtitle);
	
			graph.setOutputName(fileName);
			
			for (Entry<String, ArrayList<Tuple>> entry : myMap.entrySet()) {
			    String key = entry.getKey();
			    ArrayList<Tuple> value = entry.getValue();
				graph.addLine(value, key);
				
			}
			
			graph.draw();
		}

	}
	
	private static void special() {
		for(int i=0;i<6;i++){
			if(i==0){
				Const.CHOICE = CHOICES.AddNode ;
			}else if (i==1){
				Const.CHOICE = CHOICES.AddEdge ;
			}else if (i==2){
				Const.CHOICE = CHOICES.UpdateNode ;
			}else if (i==3){
				Const.CHOICE = CHOICES.UpdateEdge;
			}else if (i==4){
				Const.CHOICE = CHOICES.DeleteEdge ;
			}else if (i==5){
				Const.CHOICE = CHOICES.DeleteNode ;
			}
			if(Const.DB_CHOICE == Const.DB_CHOICES.ORIENTDB || Const.DB_CHOICE == Const.DB_CHOICES.ORIENTDBOSQL){
				 OrientDBBenchmark orient = new OrientDBBenchmark();
				 orient.setData2(parser.nodesEdges);
				 orient.setWeight(parser.weight);
				 orient.setAge(parser.age);
				 orient.setTestValues(parser.testValues);
				 orient.init();
				 orient.shutdown();
			}else if(Const.DB_CHOICE == Const.DB_CHOICES.TITAN){
				 TitanBenchmark titan = new TitanBenchmark();
				 titan.setData(parser.nodes,parser.edges);
				 titan.setData2(parser.nodesEdges);
				 titan.setWeight(parser.weight);
				 titan.setAge(parser.age);
				 titan.setTestValues(parser.testValues);
				 titan.init();
			}else if(Const.DB_CHOICE == Const.DB_CHOICES.MYSQL){
	
				 MysqlBenchmark mysql = new MysqlBenchmark();
				 mysql.setData2(parser.nodesEdges);
				 mysql.setWeight(parser.weight);
				 mysql.setAge(parser.age);
				 mysql.setTestValues(parser.testValues);
				 mysql.init();
			}else if(Const.DB_CHOICE == Const.DB_CHOICES.NEO4J || Const.DB_CHOICE == Const.DB_CHOICES.NEO4J_CYPHER ){
				Neo4JBenchmark neo4jCypher = new Neo4JBenchmark();
				neo4jCypher.setData2(parser.nodesEdges);
				neo4jCypher.setWeight(parser.weight);
				neo4jCypher.setAge(parser.age);
				neo4jCypher.setTestValues(parser.testValues);
				if(Const.DB_CHOICE == Const.DB_CHOICES.NEO4J)
					neo4jCypher.init();
				else
					neo4jCypher.initCypherMode();
				//neo4jCypher.shutdown();
			}
		}
	}

}
