package test;

import java.io.File;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import parser.GraphParser;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {


	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	/**
	 * Rigourous Test :-)
	 */
	public void testParser() {
		GraphParser parser = new GraphParser();
		try {
			parser.parse2(new File("").getAbsolutePath() + File.separator
					+ "data" + File.separator + "kronecker_graph_4.txt");
		} catch (Exception e) {
			e.printStackTrace();
		}

		assertTrue(parser.edges.containsKey("3:0"));
		assertTrue(parser.nodes.containsKey(2));

	}
	
	public void testApp2() {


	}
}
