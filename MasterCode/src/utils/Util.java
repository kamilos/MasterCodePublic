package utils;

import java.io.File;
import java.util.ArrayList;

public class Util {

	public static void deleteOldDB(File file) {
		if (file.exists()) {
			if (file.isDirectory()) {
				for (File child : file.listFiles()) {
					deleteOldDB(child);
				}
			}
			file.delete();
		}	
	}


}
